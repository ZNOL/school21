#pragma once

int checkRacket(int point, int player);

void moveRacket(int player, int diff, int *l, int *r);

void askPlayer(int *l, int *r);
