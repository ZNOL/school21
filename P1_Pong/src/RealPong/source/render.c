#include <stdio.h>

#include "../headers/myconstant.h"
#include "../headers/racket.h"

void printRow(int len) {
    for (int i = 0; i < len; ++i) {
        printf("‒");
    }
    printf("\n");
}

void printField(int x, int y, int l, int r, int point1, int point2) {
    printf("\033[0d\033[2J");
    printf("First: %d | Second: %d (A/Z and K/M - to move the racket)\n", point1, point2);
    printRow(width);
    for (int i = 1; i <= height; ++i) {
        for (int j = 1; j <= width; ++j) {
            if (i == y && j == x) {
                printf("○");
            } else if (j == width / 2 + 1) {
                printf("│");
            } else if ((j == 1 && checkRacket(i, l)) || (j == width && checkRacket(i, r))) {
                printf("║");
            } else {
                printf(" ");
            }
        }
        printf("\n");
    }
    printRow(width);
}
