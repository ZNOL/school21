#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "../headers/calc.h"
#include "../headers/greet.h"
#include "../headers/myconstant.h"
#include "../headers/myinput.h"
#include "../headers/racket.h"
#include "../headers/render.h"

void pong() {
    int x = width / 2;
    int y = height / 2;
    int dx = 2;
    int dy = 2;

    int l = height / 2;
    int r = height / 2;
    int point1 = 0;
    int point2 = 0;

    while (point1 < 21 && point2 < 21) {
        printField(x, y, l, r, point1, point2);

        calcChanges(&x, &y, &dx, &dy, l, r, &point1, &point2);

        askPlayer(&l, &r);

        usleep(200000);
    }

    if (point1 == 21) {
        printf("First win!\n");
    } else {
        printf("Second win!\n");
    }
}

int main() {
    srand(time(0));

    greet();

    init_keyboard();
    pong();
    close_keyboard();

    return 0;
}
