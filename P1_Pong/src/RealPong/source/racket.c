#include <stdlib.h>

#include "../headers/myconstant.h"
#include "../headers/myinput.h"

int checkRacket(int point, int player) {
    if (player - halfLen <= point && point <= player + halfLen) {
        return 1;
    } else {
        return 0;
    }
}

void moveRacket(int player, int diff, int *l, int *r) {
    int center = 0;
    if (player == 1) {
        center = *l;
    } else {
        center = *r;
    }

    if (1 <= halfLen + (center + diff) && halfLen + (center + diff) <= height) {
        if (player == 1) {
            *l += diff;
        } else {
            *r += diff;
        }
    }
}

void askPlayer(int *l, int *r) {
    char move = 'x';
    if (!kbhit()) {
        return;
    }

    move = readch();
    switch (move) {
        case 'A':
            moveRacket(1, -1, l, r);
            break;
        case 'Z':
            moveRacket(1, +1, l, r);
            break;
        case 'K':
            moveRacket(2, -1, l, r);
            break;
        case 'M':
            moveRacket(2, +1, l, r);
            break;
        case 'q':
            exit(0);
            break;
    }
}
