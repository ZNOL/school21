#include <stdlib.h>

#include "../headers/myconstant.h"
#include "../headers/racket.h"

void calcChanges(int *x, int *y, int *dx, int *dy, int l, int r, int *point1, int *point2) {
    *x += *dx;
    *y += *dy;

    if (*x >= width) {
        if (!checkRacket(*y, r)) {
            *point1 += 1;
            *x = width / 2, *y = height / 2, *dx = 1, *dy = 1;
        } else {
            *x = width;
            *dx = -(*dx);
            *y += rand() % 3;
        }
        return;
    }

    if (*x <= 1) {
        if (!checkRacket(*y, l)) {
            *point2 += 1;
            *x = width / 2, *y = height / 2, *dx = 1, *dy = 1;
        } else {
            *x = 1;
            *dx = -(*dx);
            *y += rand() % 3;
        }
        return;
    }

    if (*y < 1) {
        *y = 1;
        *y += rand() % 3;
        *dy = -(*dy);
        return;
    }
    if (*y > height) {
        *y = height;
        *y -= rand() % 3;
        *dy = -(*dy);
        return;
    }
}
