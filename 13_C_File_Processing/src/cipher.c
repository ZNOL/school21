#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getInt(int *res) {
    long double r;
    int flag = 0;
    if ((flag = scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            streamClear();
            return 0;
        } else {
            streamClear();
            return 1;
        }
    } else if (flag != -1) {
        streamClear();
        return 1;
    } else {
        return 2;
    }
}

char *readline_(FILE *stream, const char *prompt) {
    if (stream == stdin) printf("%s", prompt);

    char buf[8193] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        } else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            char *tmp = (char *)realloc(res, str_len + 1);
            if (!tmp) {
                free(res);
                return NULL;
            } else {
                res = tmp;
            }
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        } else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    } else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

long int getFileSize(FILE *fp) {
    fseek(fp, 0, SEEK_END);
    long long int res = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return res;
}

int readfile(FILE *file) {
    int sz = getFileSize(file);
    if (sz == 0) {
        return 1;
    }

    char *str = (char *)calloc(sz + 1, sizeof(char));

    fseek(file, 0, SEEK_SET);
    fread(str, sizeof(char), sz, file);

    printf("%s", str);
    free(str);

    return 0;
}

int writefile(FILE *file, char *str) {
    fseek(file, 0, SEEK_END);
    fwrite(str, sizeof(char), strlen(str), file);

    return 0;
}

void diff_file(FILE *file, int diff) {
    int sz = getFileSize(file);
    
    for (int i = 0; i < sz; ++i) {
        char c;
        fseek(file, sizeof(char) * i, SEEK_SET);
        fread(&c, sizeof(char), 1, file);
        fseek(file, sizeof(char) * i, SEEK_SET);
        c = (((c + diff) % 128) + 128) % 128;
        fwrite(&c, sizeof(char), 1, file);
    }
}

void encrypt(char *dir_name, int diff) {
    DIR *d = opendir(dir_name);
    struct dirent *dir;
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strstr(dir->d_name, ".c") != NULL) {
                char fullname[300] = {0};
                strcat(fullname, dir_name);
                strcat(fullname, dir->d_name);
                FILE *file = fopen(fullname, "r+");
                if (file) {
                    diff_file(file, diff);
                    fclose(file);
                }
            }
            else if (strstr(dir->d_name, ".h") != NULL) {
                char fullname[300] = {0};
                strcat(fullname, dir_name);
                strcat(fullname, dir->d_name);
                fclose(fopen(fullname, "w"));
            }
        }
        closedir(d);
    }
}

int main(int argc, char **argv) {
    int flag = 1, need_n = 0;
    char name[101] = {0};
    while (flag) {
        int x = -1;
        int wasDigit = getInt(&x);
        if (wasDigit == 0) {
            if (x == 1) {
                if (need_n) {
                    printf("\n");
                }
                need_n = 1;

                if (scanf("%100s", name) <= 0) {
                    printf("n/a");
                    continue;
                }

                FILE *file = fopen(name, "r");
                if (file) {
                    if (readfile(file)) {
                        printf("n/a");
                    }
                    fclose(file);
                } else {
                    printf("n/a");
                }
            } else if (x == 2) {
                if (need_n) {
                    printf("\n");
                }
                need_n = 1;

                FILE *file = fopen(name, "r+");
                char *str = readline_(stdin, "");
                if (file) {
                    if (writefile(file, str)) {
                        printf("n/a");
                    }
                    readfile(file);
                    fclose(file);
                } else {
                    printf("n/a");
                }
                free(str);
            } else if (x == 3) {
                char *name = readline_(stdin, "");
                if (argc == 2) {
                    encrypt(name, atoi(argv[1])); 
                }
                else {
                    if (need_n) {
                        printf("\n");
                    }
                    printf("n/a");
                }
                free(name);
            } else {
                flag = 0;
            }
        } else if (wasDigit == 1) {
            printf("n/a");
        } else {
            flag = 0;
        }
    }

    return 0;
}
