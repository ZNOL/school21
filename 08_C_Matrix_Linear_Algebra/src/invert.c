#include <stdio.h>
#include <stdlib.h>

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int getDouble(double *res) {
    if ((scanf("%lf", res)) == 1) {
        return 0;
    } else {
        return 1;
    }
}

double **initMatrix(int n, int m) {
    double **matrix = (double **)malloc(n * sizeof(double *));
    for (int i = 0; i < n; ++i) {
        matrix[i] = (double *)malloc(m * sizeof(double));
    }
    return matrix;
}

void clearMatrix(double **matrix, int n) {
    for (int i = 0; i < n; ++i) {
        free(matrix[i]);
    }
    free(matrix);
}

double **input(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    double **matrix = initMatrix(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getDouble(&matrix[i][j])) {
                clearMatrix(matrix, *n);
                return NULL;
            }
        }
    }

    return matrix;
}

void output(double **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%lf", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

double **makeEMatrix(int n) {
    double **res = initMatrix(n, n);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (i == j) {
                res[i][j] = 1;
            } else {
                res[i][j] = 0;
            }
        }
    }
    return res;
}

void swap(double *a, double *b) {
    double tmp = *a;
    *a = *b;
    *b = tmp;
}

void swapRows(double **matrix, int m, int i1, int i2) {
    for (int j = 0; j < m; ++j) {
        swap(&matrix[i1][j], &matrix[i2][j]);
    }
}

void multRow(double **matrix, int m, int i, double x) {
    for (int j = 0; j < m; ++j) {
        matrix[i][j] *= x;
    }
}

void addRow(double **matrix, int m, int dest_i, int src_i, double x) {
    for (int j = 0; j < m; ++j) {
        matrix[dest_i][j] += x * matrix[src_i][j];
    }
}

double **invert(double **matrix, int n) {
    double **result = makeEMatrix(n);

    for (int i = 0; i < n; ++i) {
        if (matrix[i][i] != 0) {
            double x = 1.0 / matrix[i][i];
            multRow(matrix, n, i, x);
            multRow(result, n, i, x);

            matrix[i][i] = 1;

            for (int i2 = i + 1; i2 < n; ++i2) {
                if (matrix[i2][i] != 0) {
                    double x = -matrix[i2][i];
                    addRow(matrix, n, i2, i, x);
                    addRow(result, n, i2, i, x);

                    matrix[i2][i] = 0;
                }
            }
        } else {
            clearMatrix(result, n);
            return NULL;
        }
    }

    for (int i = n - 1; i >= 0; --i) {
        if (matrix[i][i] != 0) {
            for (int i2 = i - 1; i2 >= 0; --i2) {
                double x = -matrix[i2][i];
                addRow(matrix, n, i2, i, x);
                addRow(result, n, i2, i, x);

                matrix[i2][i] = 0;
            }
        } else {
            clearMatrix(result, n);
            return NULL;
        }
    }

    return result;
}

int main() {
    int n = 0, m = 0;
    double **matrix = input(&n, &m);
    if (matrix == NULL) {
        printf("n/a");
        return 0;
    }

    if (n != m) {
        clearMatrix(matrix, n);
        printf("n/a");
        return 0;
    }

    double **result = invert(matrix, n);

    if (result == NULL) {
        clearMatrix(matrix, n);
        printf("n/a");
        return 0;
    }

    output(result, n, n);

    clearMatrix(matrix, n);
    clearMatrix(result, n);

    return 0;
}
