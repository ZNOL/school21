#include <stdio.h>
#include <stdlib.h>

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int getDouble(double *res) {
    if ((scanf("%lf", res)) == 1) {
        return 0;
    } else {
        return 1;
    }
}

double **initMatrix(int n, int m) {
    double **matrix = (double **)malloc(n * sizeof(double *));
    for (int i = 0; i < n; ++i) {
        matrix[i] = (double *)malloc(m * sizeof(double));
    }
    return matrix;
}

void clearMatrix(double **matrix, int n) {
    for (int i = 0; i < n; ++i) {
        free(matrix[i]);
    }
    free(matrix);
}

double **input(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    double **matrix = initMatrix(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getDouble(&matrix[i][j])) {
                clearMatrix(matrix, *n);
                return NULL;
            }
        }
    }

    return matrix;
}

void output(double **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%lf", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

double **makeMinor(double **matrix, int n, int forbidden) {
    double **res = initMatrix(n - 1, n - 1);
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (i == 0 || j == forbidden) {
                continue;
            }

            int out_i = i, out_j = j;
            if (i > 0) {
                out_i--;
            }
            if (j > forbidden) {
                out_j--;
            }
            res[out_i][out_j] = matrix[i][j];
        }
    }
    return res;
}

double det(double **matrix, int n) {
    double ans = 0;
    if (n == 1) {
        return matrix[0][0];
    }

    for (int j = 0; j < n; ++j) {
        int x = (j % 2 == 0) ? 1 : -1;

        double **temp_matrix = makeMinor(matrix, n, j);

        ans += x * matrix[0][j] * det(temp_matrix, n - 1);

        clearMatrix(temp_matrix, n - 1);
    }

    return ans;
}

int main() {
    int n = 0, m = 0;
    double **matrix = input(&n, &m);
    if (matrix == NULL) {
        printf("n/a");
        return 0;
    }

    if (n != m) {
        clearMatrix(matrix, n);
        printf("n/a");
        return 0;
    }

    printf("%.6lf", det(matrix, n));

    clearMatrix(matrix, n);

    return 0;
}
