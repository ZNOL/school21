#include <stdio.h>
#include <stdlib.h>

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int **initMatrix(int n, int m) {
    int **matrix = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; ++i) {
        matrix[i] = (int *)malloc(m * sizeof(int));
    }
    return matrix;
}

void clearMatrix(int **matrix, int n) {
    for (int i = 0; i < n; ++i) {
        free(matrix[i]);
    }
    free(matrix);
}

int **input(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    int **matrix = initMatrix(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&matrix[i][j])) {
                clearMatrix(matrix, *n);
                return NULL;
            }
        }
    }

    return matrix;
}

void output(int **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int *initArray(int n) { return (int *)malloc(n * sizeof(int)); }

void clearArray(int *arr) { free(arr); }

int *genArray(int **matrix, int n, int m) {
    int *res = initArray(n * m);
    int idx = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            res[idx++] = matrix[i][j];
        }
    }
    return res;
}
/*
    1 6 7
    2 5 8
    3 4 9
*/
void sort_vertical(int *arr, int n, int m, int **result_matrix) {
    int idx = 0;
    for (int j = 0; j < m; ++j) {
        if (j % 2 == 0) {
            for (int i = 0; i < n; ++i) {
                result_matrix[i][j] = arr[idx++];
            }
        } else {
            for (int i = n - 1; i >= 0; --i) {
                result_matrix[i][j] = arr[idx++];
            }
        }
    }
}

/*
    1 2 3
    6 5 4
    7 8 9
*/
void sort_horizontal(int *arr, int n, int m, int **result_matrix) {
    int idx = 0;
    for (int i = 0; i < n; ++i) {
        if (i % 2 == 0) {
            for (int j = 0; j < m; ++j) {
                result_matrix[i][j] = arr[idx++];
            }
        } else {
            for (int j = m - 1; j >= 0; --j) {
                result_matrix[i][j] = arr[idx++];
            }
        }
    }
}

int cmp(const void *a, const void *b) {
    int va = *(int *)a, vb = *(int *)b;
    return va < vb ? -1 : va > vb ? +1 : 0;
}

int main() {
    int n, m;
    int **matrix = NULL, **result = NULL;

    matrix = input(&n, &m);
    if (matrix == NULL) {
        printf("n/a");
        return 0;
    }

    int *temp = genArray(matrix, n, m);
    qsort(temp, n * m, sizeof(int), cmp);

    result = initMatrix(n, m);

    sort_vertical(temp, n, m, result);
    output(result, n, m);

    printf("\n\n");

    sort_horizontal(temp, n, m, result);
    output(result, n, m);

    clearArray(temp);
    clearMatrix(matrix, n);
    clearMatrix(result, n);

    return 0;
}
