#include "s21_string.h"

#include <stdio.h>
#include <stdlib.h>

int s21_strlen_test() {
    char *tests[] = {
        "\0",
        "Test",
        "Aaaaaa",
        NULL,
    };
    const int res[] = {0, 4, 6, -1};
    const int N = sizeof(tests) / sizeof(tests[0]);

    int flag = 0;
    for (int n = 0; n < N; ++n) {
        int my_res = s21_strlen(tests[n]);
        printf("Input: \"%10s\" | Output: %2d | ", tests[n], my_res);
        if (my_res == res[n]) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            flag++;
        }
    }
    return flag;
}

int s21_strcmp_test() {
    char *tests[] = {
        "aaab", "caas", "", "assad", "bba", "asas", "str", NULL,
    };
    const int res[] = {-2, -97, 1, -1};
    const int N = sizeof(tests) / sizeof(tests[0]);

    int flag = 0;
    for (int n = 0; n < N; n += 2) {
        int my_res = s21_strcmp(tests[n], tests[n + 1]);
        printf("Input: \"%10s\" \"%10s\" | Output %3d | ", tests[n], tests[n + 1], my_res);
        if (my_res == res[n / 2]) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            flag++;
        }
    }
    return flag;
}

int s21_strcpy_test() {
    char *tests[] = {
        "asdasdasd", "xxx", "", "dwm", NULL, NULL,
    };
    char *res[] = {"xxx", "dwm", NULL};

    int flag = 0;
    const int N = sizeof(tests) / sizeof(tests[0]);
    for (int n = 0; n < N; n += 2) {
        char *output = NULL;
        if (tests[n]) {
            output = (char *)calloc(s21_strlen(tests[n]) + 1, sizeof(char));
        }

        s21_strcpy(output, tests[n]);
        s21_strcpy(output, tests[n + 1]);
        printf("Input: \"%10s\" \"%10s\" | Output \"%20s\" | ", tests[n], tests[n + 1], output);
        if (s21_strcmp(output, res[n / 2]) == 0) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            ++flag;
        }
        free(output);
    }
    return flag;
}

int s21_strcat_test() {
    char *tests[] = {
        "a", "xx", "", "aaa", NULL, NULL,
    };
    char *res[] = {"axx", "aaa", NULL};
    const int N = sizeof(tests) / sizeof(tests[0]);

    int flag = 0;
    for (int n = 0; n < N; n += 2) {
        char *output = (char *)calloc(s21_strlen(tests[n]) + s21_strlen(tests[n + 1]) + 1, sizeof(char));
        s21_strcat(output, tests[n]);
        s21_strcat(output, tests[n + 1]);

        printf("Input: \"%10s\" \"%10s\" | Output \"%10s\" | ", tests[n], tests[n + 1], output);
        if (s21_strcmp(output, res[n / 2]) == 0) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            ++flag;
        }
        free(output);
    }
    return flag;
}

int s21_strchr_test() {
    char *tests[] = {
        "aaaabaaaa", "b", "asdasdasd", "c", NULL, "a",
    };
    char res[] = {'b', 0, 0};
    const int N = sizeof(tests) / sizeof(tests[0]);

    int flag = 0;
    for (int n = 0; n < N; n += 2) {
        char *my_res = s21_strchr(tests[n], tests[n + 1][0]);
        printf("Input: \"%10s\" \"%c\" | Output \"%6s\" | ", tests[n], tests[n + 1][0], my_res);
        if ((my_res == NULL && res[n / 2] == 0) || (my_res != NULL && *(my_res) == res[n / 2])) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            ++flag;
        }
    }
    return flag;
}

int s21_strstr_test() {
    char *tests[] = {
        "abacaba", "bac", "aaaa", "bb", "abrrba", "rba",
    };
    char *res[] = {"bacaba", NULL, "rba"};
    const int N = sizeof(tests) / sizeof(tests[0]);

    int flag = 0;
    for (int n = 0; n < N; n += 2) {
        char *my_res = s21_strstr(tests[n], tests[n + 1]);
        printf("Input: \"%10s\" \"%10s\" | Output \"%6s\" | ", tests[n], tests[n + 1], my_res);
        if ((my_res == NULL && res[n / 2] == NULL) ||
            (my_res != NULL && s21_strcmp(my_res, res[n / 2]) == 0)) {
            printf("SUCCES\n");
        } else {
            printf("FAIL\n");
            ++flag;
        }
    }
    return flag;
}

int s21_strtok_test() {
    char test[] = "word1 word2  -   word3---word4____word5";
    char *res[] = {"word1", "word2", "word3", "word4", "word5"};

    printf("Input: %20s | Outputs: ", test);
    int idx = 0, flag = 0;
    char *word = s21_strtok(test, " -_");
    while (word) {
        printf("\"%s\" ", word);
        if (s21_strcmp(word, res[idx++]) == 0) {
            printf("(SUCCES) ");
        } else {
            printf("(FAIL) ");
            ++flag;
        }
        word = s21_strtok(NULL, " -_");
    }
    printf("\n");

    return flag;
}

int main() {
    int (*fptr[])() = {
#ifdef Q1
        s21_strlen_test,
#endif  // Q1
#ifdef Q2
        s21_strcmp_test,
#endif  // Q2
#ifdef Q3
        s21_strcpy_test,
#endif  // Q3
#ifdef Q4
        s21_strcat_test,
#endif  // Q4
#ifdef Q5
        s21_strchr_test,
#endif  // Q5
#ifdef Q6
        s21_strstr_test,
#endif  // Q6
#ifdef Q7
        s21_strtok_test,
#endif  // Q7
    };
    int N = sizeof(fptr) / sizeof(fptr[0]);

    for (int i = 0; i < N; ++i) {
        fptr[i]();
        printf("\n----------------------------------------------------------\n\n");
    }

    return 0;
}
