#ifndef SRC_S21_STRING_H_
#define SRC_S21_STRING_H_

int s21_strlen(char *str);
int s21_strcmp(char *first, char *second);
char *s21_strcpy(char *dst, char *src);
char *s21_strcat(char *dst, char *src);
char *s21_strchr(char *s, char c);
char *s21_strstr(char *haystack, char *needle);
char *s21_strtok(char *str, char *sep);

#endif  // SRC_S21_STRING_H_
