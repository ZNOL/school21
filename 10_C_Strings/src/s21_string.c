#include <stdio.h>
#include <stdlib.h>

int s21_strlen(char *str) {
    int ans = -1;
    if (str) {
        ans = 0;
        for (; str[ans]; ++ans) {
        }
    }
    return ans;
}

int s21_strcmp(char *first, char *second) {
    if (first == NULL || second == NULL) {
        return (first == second) ? 0 : -1;
    }

    int i1 = 0, i2 = 0;
    while (first[i1] && second[i2] && first[i1] == second[i2]) {
        ++i1;
        ++i2;
    }

    int ans = 0;
    if (first[i1] != second[i2]) {
        ans = first[i1] - second[i2];
    }
    return ans;
}

char *s21_strcpy(char *dst, char *src) {
    if (dst == NULL || src == NULL) {
        return NULL;
    }
    int i = 0;
    for (; src[i]; ++i) {
        dst[i] = src[i];
    }
    dst[i] = src[i];
    return dst;
}

char *s21_strcat(char *dst, char *src) {
    if (dst == NULL || src == NULL) {
        return NULL;
    }

    int idx = s21_strlen(dst), i = 0;
    for (; src[i]; ++i, ++idx) {
        dst[idx] = src[i];
    }
    dst[idx] = src[i];
    return dst;
}

char *s21_strchr(char *s, char c) {
    if (s == NULL) {
        return NULL;
    }

    while (*s && *s != c) {
        ++s;
    }

    return (*s) ? s : (c == '\0') ? s : NULL;
}

char *s21_strstr(char *haystack, char *needle) {
    if (haystack == NULL || needle == NULL) {
        return NULL;
    }

    char *res = NULL;
    int needle_len = s21_strlen(needle);
    for (int i = 0; haystack[i]; ++i) {
        res = (char *)(haystack + i);
        int j;
        for (j = 0; needle[j] && res[j] && res[j] == needle[j]; ++j) {
        }
        if (j == needle_len) {
            return res;
        }
    }

    return NULL;
}

char *s21_strtok(char *str, char *sep) {
    static char *next;

    if (str) {
        next = str;
        while (*next && s21_strchr(sep, *next)) {
            *next++ = '\0';
        }
    }

    if (*next == '\0') return NULL;

    str = next;
    while (*next && !s21_strchr(sep, *next)) {
        ++next;
    }
    while (*next && s21_strchr(sep, *next)) {
        *next++ = '\0';
    }

    return str;
}
