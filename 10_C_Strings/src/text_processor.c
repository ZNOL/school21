#include <stdio.h>
#include <stdlib.h>

#include "s21_string.h"

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

char *input() {
    char *str = (char *)calloc(101, sizeof(char));
    char c;
    int idx = 0;
    while (scanf("%c", &c) == 1 && idx < 100) {
        if (c == '\n') {
            break;
        }
        str[idx++] = c;
    }
    str[idx] = '\0';
    return str;
}

char *initStr(int n) { return (char *)calloc(n, sizeof(char)); }

void clearStr(char *s) {
    if (s) {
        free(s);
    }
}

char *clearSpaces(char *inp) {
    char *res = initStr(1000);
    int need_space = 0;
    char *word = s21_strtok(inp, " ");
    while (word) {
        if (need_space == 1) {
            s21_strcat(res, " ");
        }
        s21_strcat(res, word);
        need_space = 1;
        word = s21_strtok(NULL, " ");
    }
    return res;
}

void insert(char *str, int idx, char c) {
    char *copy = initStr(1000);
    s21_strcat(copy, str);
    s21_strcpy(&str[idx + 1], &copy[idx]);
    str[idx] = c;

    clearStr(copy);
}

void delete (char *str, int idx) { s21_strcpy(&str[idx - 1], &str[idx]); }

char *processing(char *inp, int width) {
    char *res = clearSpaces(inp);

    for (int end = width; end < s21_strlen(res); end += width + 1) {
        if (res[end] == ' ' || res[end - 1] == ' ') {
            insert(res, end, '\n');
        } else if (res[end + 1] == ' ') {
            insert(res, end - 1, ' ');
            insert(res, end, '\n');
        } else {
            insert(res, end - 1, '-');
            insert(res, end, '\n');
        }

        while (end - width >= 0 && res[end - width] == ' ') {
            delete (res, end - width);
        }
    }

    return res;
}

int main(int argc, char **argv) {
    if (!(argc == 2 && s21_strcmp(argv[1], "-w") == 0)) {
        printf("n/a");
        return 0;
    }

    int width = 0;
    int flag = getInt(&width);
    scanf("%*c");
    char *inp = input();
    if (flag || s21_strlen(inp) == 0 || width < 1) {
        clearStr(inp);
        printf("n/a");
        return 0;
    }

    char *res = processing(inp, width);
    printf("%s", res);

    clearStr(inp);
    clearStr(res);

    return 0;
}
