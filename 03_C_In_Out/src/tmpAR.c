#include <stdio.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getInt(int *ans) {
    int flag = 0;
    printf("Input number: ");
    while ((flag = scanf("%d", ans)) != 1) {
        if (flag == -1) {
            *ans = 0;
            return 1;
        }
        printf("Error while input integer\nTry again\n");
        scanf("%*[^\n]");
    }
    streamClear();

    return 0;
}

int sum(int a, int b) { return a + b; }

int subtraction(int a, int b) { return a - b; }

int product(int a, int b) { return a * b; }

int division(int a, int b) { return a / b; }

int main() {
    int a, b;

    if (getInt(&a)) {
        printf("Ошибка при считывании\n");
        return 1;
    }
    if (getInt(&b)) {
        printf("Ошибка при считывании\n");
        return 1;
    }

    printf("Введены числа: %d %d\n", a, b);
    if (b != 0) {
        printf("%d %d %d %d\n", sum(a, b), subtraction(a, b), product(a, b), division(a, b));
    } else {
        printf("%d %d %d n/a\n", sum(a, b), subtraction(a, b), product(a, b));
    }

    return 0;
}
