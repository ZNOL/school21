#include <math.h>
#include <stdio.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getDouble(double *ans) {
    int flag = 0;
    char tmp = ' ';
    if ((flag = scanf("%lf%c", ans, &tmp)) == 2 && tmp == '\n') {
        return 0;
    }
    *ans = 0;
    return 1;
}

double calculate(double x) {
    return 7 * pow(10, -3) * pow(x, 4) + ((22.8 * pow(x, 1.0 / 3) - pow(10, 3)) * x + 3) / (x * x / 2) -
           x * pow(10 + x, 2.0 / x) - 1.01;
}

int main() {
    double a;

    if (getDouble(&a) || a == 0) {
        printf("n/a\n");
        return 1;
    }

    //    printf("Введенное значение: %lf\n", a);
    printf("%.1lf\n", calculate(a));

    return 0;
}
