#include <stdio.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getTwoIntegers(int *a, int *b) {
    int flag = scanf("%d %d", a, b);
    if (flag == 2) {
        char tmp = ' ';
        if ((scanf("%c", &tmp)) != 0 && tmp != '\n') {
            *a = 0, *b = 0;
            streamClear();
            return 1;
        } else {
            return 0;
        }
    } else if (flag == -1) {
        *a = 0, *b = 0;
        return -1;
    } else {
        streamClear();
        return 1;
    }
}

int findMax(int a, int b) {
    //    if (a == b) {
    //        printf("Integers are equal\n");
    //    }
    return (a < b) ? b : a;
}

int main() {
    int a, b;
    if (getTwoIntegers(&a, &b)) {
        printf("n/a\n");
        return 1;
    }

    //    printf("Введены числа: %d %d\n", a, b);
    printf("%d\n", findMax(a, b));

    return 0;
}
