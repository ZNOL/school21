#include <stdio.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getTwoDouble(long double *a, long double *b) {
    int flag = scanf("%Lf %Lf", a, b);
    if (flag == 2) {
        char tmp = ' ';
        if ((scanf("%c", &tmp)) != 0 && tmp != '\n') {
            *a = 0, *b = 0;
            streamClear();
            return 1;
        } else {
            return 0;
        }
    } else if (flag == -1) {
        *a = 0, *b = 0;
        return -1;
    } else {
        streamClear();
        return 1;
    }
}

const double EPS = 1e-6;

int main() {
    long double x, y;
    if (getTwoDouble(&x, &y)) {
        printf("n/a\n");
        return 1;
    }

    long double res = x * x + y * y;
    long double val = 25.0;

    //    printf("%lf\n%lf\n", res, val);
    if (res < val && val - res >= EPS) {
        printf("GOTCHA\n");
    } else {
        printf("MISS\n");
    }

    return 0;
}
