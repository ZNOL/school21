#include <stdio.h>

void streamClear() {
    scanf("%*[^\n]");
    scanf("%*c");
}

int getTwoIntegers(int *a, int *b) {
    int flag = scanf("%d %d", a, b);
    if (flag == 2) {
        char tmp = ' ';
        if ((scanf("%c", &tmp)) != 0 && tmp != '\n') {
            *a = 0, *b = 0;
            streamClear();
            return 1;
        } else {
            return 0;
        }
    } else if (flag == -1) {
        *a = 0, *b = 0;
        return -1;
    } else {
        streamClear();
        return 1;
    }
}

int sum(int a, int b) { return a + b; }

int subtraction(int a, int b) { return a - b; }

int product(int a, int b) { return a * b; }

int division(int a, int b) { return a / b; }

int main() {
    int a, b;
    if (getTwoIntegers(&a, &b)) {
        printf("n/a\n");
        return 1;
    }

    //    printf("Введены числа: %d %d\n", a, b);
    if (b != 0) {
        printf("%d %d %d %d\n", sum(a, b), subtraction(a, b), product(a, b), division(a, b));
    } else {
        printf("%d %d %d n/a\n", sum(a, b), subtraction(a, b), product(a, b));
    }

    return 0;
}
