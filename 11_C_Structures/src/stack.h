#ifndef SRC_STACK_H_
#define SRC_STACK_H_

typedef struct Node {
    int x;
    struct Node *next;
} Node;


typedef struct Stack {
    Node *top;
} Stack;

Stack *initStack();
Node *initNode(const int x); 
void clearNode(Node *ptr); 
void clearStack(Stack *stack); 
int top(Stack *stack); 
void push(Stack *stack, int x); 
void pop(Stack *stack);

#endif  // SRC_STACK_H_
