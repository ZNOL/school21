#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "door_struct.h"

#define DOORS_COUNT 15
#define MAX_ID_SEED 10000

void initialize_doors(Door *doors);

void output(Door *doors, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d, %d", doors[i].id, doors[i].status);
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int cmp(const void *a, const void *b) {
    Door A = *(Door *)a, B = *(Door *)b;
    return A.id - B.id;
}

void door_closing(Door *doors, int n) {
    for (int i = 0; i < n; ++i) {
        doors[i].status = 0;
    }
}

int main() {
    Door doors[DOORS_COUNT];

    initialize_doors(doors);

    qsort(doors, DOORS_COUNT, sizeof(Door), cmp);

    door_closing(doors, DOORS_COUNT);
    output(doors, DOORS_COUNT);

    return 0;
}

// Doors initialization function
// ATTENTION!!!
// DO NOT CHANGE!
void initialize_doors(Door *doors) {
    srand(time(0));

    int seed = rand() % MAX_ID_SEED;
    for (int i = 0; i < DOORS_COUNT; i++) {
        doors[i].id = (i + seed) % DOORS_COUNT;
        doors[i].status = rand() % 2;
    }
}
