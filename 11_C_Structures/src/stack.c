#include "stack.h"

#include <stdlib.h>

Stack *initStack() { return (Stack *)calloc(1, sizeof(Stack)); }

Node *initNode(const int x) {
    Node *res = (Node *)calloc(1, sizeof(Node));
    res->x = x;
    return res;
}

void clearNode(Node *ptr) { free(ptr); }

void clearStack(Stack *stack) {
    Node *ptr = stack->top;
    while (ptr) {
        Node *tmp = ptr->next;
        clearNode(ptr);
        ptr = tmp;
    }
    free(stack);
}

int top(Stack *stack) {
    int result = -1;
    if (stack->top) {
        result = stack->top->x;
    }
    return result;
}

void push(Stack *stack, int x) {
    Node *new_node = initNode(x);
    new_node->next = stack->top;
    stack->top = new_node;
}

void pop(Stack *stack) {
    Node *ptr = stack->top;
    if (ptr != NULL) {
        stack->top = ptr->next;
        clearNode(ptr);
    }
}
