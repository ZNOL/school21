#include "list.h"

#include <stdio.h>
#include <stdlib.h>

int addDoor_test() {
    Door t[] = {
        {.id = 12, .status = 1},
        {.id = 14, .status = 0},
        {.id = 15, .status = 1},
    };
    int N = sizeof(t) / sizeof(t[0]);

    Node *head = NULL;
    for (int i = 0; i < N; ++i) {
        addDoor(&head, &t[i]);
    }

    int flag = 0;
    for (int i = 0; i < N; ++i) {
        Node *ptr = findDoor(t[i].id, head);
        if (ptr == NULL || ptr->door.id != t[i].id) {
            ++flag;
        }
    }

    clearList(head);
    return flag;
}

int removeDoor_test() {
    Door t[] = {
        {.id = 12, .status = 1},
        {.id = 14, .status = 0},
        {.id = 15, .status = 1},
    };
    int N = sizeof(t) / sizeof(t[0]);

    Node *head = NULL;
    for (int i = 0; i < N; ++i) {
        addDoor(&head, &t[i]);
    }

    for (int i = N - 1; i >= 0; --i) {
        removeDoor(findDoor(t[i].id, head), &head);
    }

    return (head == NULL) ? 0 : 1;
}

int main() {
    int (*fptr[])() = {
        addDoor_test,
        removeDoor_test,
    };
    int N = sizeof(fptr) / sizeof(fptr[0]);

    char *result[] = {"SUCCES\n", "FAIL\n"};
    for (int n = 0; n < N; ++n) {
        int res = fptr[n]();
        printf("%s", result[res]);
        printf("\n----------------------------------------------------------\n\n");
    }

    return 0;
}
