#include "list.h"

#include <stdio.h>
#include <stdlib.h>

Node *init(const Door *door) {
    Node *nw = (Node *)malloc(sizeof(Node));
    nw->door = *door;
    nw->next = NULL;
    return nw;
}

void clearNode(Node *ptr) { free(ptr); }

void clearList(Node *head) {
    Node *ptr = head;
    while (ptr) {
        Node *tmp = ptr->next;
        clearNode(ptr);
        ptr = tmp;
    }
}

void addDoor(Node **head, Door *door) {
    Node *nw = init(door);

    if (*head == NULL) {
        *head = nw;
    } else {
        nw->next = (*head)->next;
        (*head)->next = nw;
    }
}

Node *findDoor(int door_id, Node *root) {
    Node *ptr = root;
    while (ptr->door.id != door_id) {
        ptr = ptr->next;
    }
    return ptr;
}

void removeDoor(Node *elem, Node **root) {
    Node *ptr = *root;
    Node *prev = NULL;

    while (ptr != elem) {
        prev = ptr;
        ptr = ptr->next;
    }

    if (prev == NULL) {
        *root = ptr->next;
    } else {
        prev->next = ptr->next;
    }

    clearNode(ptr);
}
