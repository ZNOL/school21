#include "stack.h"

#include <stdio.h>

int push_test() {
    int values[] = {1, 5, 2, 4, 1, 5};
    int N = sizeof(values) / sizeof(values[0]);

    Stack *s = initStack();

    int flag = 0;
    for (int i = 0; i < N; ++i) {
        push(s, values[i]);
        if (top(s) != values[i]) {
            flag++;
        }
    }

    clearStack(s);
    return flag;
}

int pop_test() {
    int values[] = {2, 5, 2, 5, 7};
    int N = sizeof(values) / sizeof(values[0]);

    Stack *s = initStack();

    for (int i = 0; i < N; ++i) {
        push(s, values[i]);
    }

    int flag = 0;
    for (int i = N - 1; i >= 0; --i) {
        int v = top(s);
        pop(s);
        if (v != values[i] || top(s) == v) {
            ++flag;
        }
    }

    clearStack(s);
    return flag;
}

int main() {
    int (*fptr[])() = {
        push_test,
        pop_test,
    };
    const int N = sizeof(fptr) / sizeof(fptr[0]);

    char *result[] = {"SUCCES\n", "FAIL\n"};
    for (int n = 0; n < N; ++n) {
        int res = fptr[n]();
        printf("%s", result[res]);
        printf("\n----------------------------------------------------------\n\n");
    }

    return 0;
}
