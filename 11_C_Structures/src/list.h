#ifndef SRC_LIST_H_
#define SRC_LIST_H_

#include "door_struct.h"

typedef struct Node {
    Door door;
    struct Node *next;
} Node;

Node *init(const Door *door);
void clearNode(Node *ptr);
void clearList(Node *head);
void addDoor(Node **head, Door *door);
Node *findDoor(int door_id, Node *root);
void removeDoor(Node *elem, Node **root);

#endif  // SRC_LIST_H_
