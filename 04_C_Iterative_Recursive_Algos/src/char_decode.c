#include <math.h>
#include <stdio.h>

void getHex(int x) {
  if (x == 0) {
    return;
  }

  getHex(x / 16);
  int digit = x % 16;
  if (digit < 10) {
    printf("%d", digit);
  } else {
    printf("%c", ('A' + (digit - 10)));
  }
}

int encode() {
  char c, del;
  int flag = 0;
  while ((scanf("%c%c", &c, &del)) == 2) {
    if (del != ' ' && del != '\n') {
      printf("n/a\n");
      return 1;
    }
    getHex(c);
    printf(" ");
    if (del == '\n') {
      flag = 1;
      break;
    }
  }
  if (flag) {
    printf("\n");
  } else {
    printf("n/a\n");
  }

  return 0;
}

int decode() {
  int x;
  char del;
  int flag = 0;
  while ((scanf("%x%c", &x, &del)) == 2) {
    if (!(0 <= x && x < 128) || (del != ' ' && del != '\n')) {
      printf("n/a\n");
      return 1;
    }

    printf("%c ", x);
    if (del == '\n') {
      flag = 1;
      break;
    }
  }
  if (flag) {
    printf("\n");

  } else {
    printf("n/a\n");
  }

  return 0;
}

int main(int argc, char **argv) {
  ++argc;
  char flag = argv[1][0];
  if (flag == '0') {
    encode();
  } else if (flag == '1') {
    decode();
  }

  return 0;
}
