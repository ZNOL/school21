#include <math.h>
#include <stdio.h>

typedef long double ld;

const ld pi = 3.141592653589793238462;
const ld step = 2 * pi / 41;
// isnan

ld VA(ld x) { return (ld)1 / ((ld)1 + x * x); }

ld LB(ld x) { return sqrt(sqrt(1 + 4 * x * x) - x * x - 1); }

ld QH(ld x) { return (ld)1 / (x * x); }

void printVal(ld x) {
  if (!isnan(x)) {
    printf("%.7Lf", x);
  } else {
    printf("-");
  }
}

void calc() {
  ld x = -pi;
  for (int i = 0; i < 42; ++i, x += step) {
    ld a = VA(x);
    ld b = LB(x);
    ld c = QH(x);
    printf("%.7Lf | ", x);
    printVal(a);
    printf(" | ");
    printVal(b);
    printf(" | ");
    printVal(c);
    printf("\n");
  }
}

int main() {
  calc();

  return 0;
}
