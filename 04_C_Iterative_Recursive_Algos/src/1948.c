#include <stdio.h>

#define max(a, b) (a < b) ? b : a

void streamClear() {
  scanf("%*[^\n]");
  scanf("%*c");
}

int getInt(int *ans) {
  int flag = 0;
  char tmp = ' ';
  if ((flag = scanf("%d%c", ans, &tmp)) == 2 && tmp == '\n') {
    return 0;
  } else {
    streamClear();
    return 1;
  }
}

int divmod(int x, int y, int *d, int *m) {
  *d = 0, *m = 0;
  while (x >= y) {
    x -= y;
    (*d)++;
  }
  *m = x;

  return 0;
}

int gpd(int a) {
  int i = 2;
  int ans = -1;
  while (a > 1) {
    int div = -1, mod = -1;
    divmod(a, i, &div, &mod);
    while (mod == 0) {
      ans = max(ans, i);
      a = div;
      // printf("%d ", i);
      divmod(a, i, &div, &mod);
    }
    i++;
  }
  // printf("\n");
  return ans;
}

int main() {
  int a = 0;

  if (getInt(&a) || (a == -1 || a == 0 || a == 1)) {
    printf("n/a\n");
    return 0;
  }

  if (a < 0) {
    a = -a;
  }

  printf("%d\n", gpd(a));

  return 0;
}
