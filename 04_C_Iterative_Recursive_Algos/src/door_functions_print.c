#include <math.h>
#include <stdio.h>

typedef long double ld;

const ld pi = 3.141592653589793238462;
const ld step = 2 * pi / 41;
// isnan

ld VA(ld x) { return (ld)1 / ((ld)1 + x * x); }

ld LB(ld x) { return sqrt(sqrt(1 + 4 * x * x) - x * x - 1); }

ld QH(ld x) { return (ld)1 / (x * x); }

void printVal(ld x) {
  if (!isnan(x)) {
    printf("%.7Lf", x);
  } else {
    printf("-");
  }
}

void printStar(ld x, ld step) {
  long long int count = x / step;
  for (int i = 0; i < count; ++i) {
    printf("*");
  }
  printf("\n");
}

void calc1() {
  ld x = -pi;
  // ld mn = 1000;
  // ld mx = -1000;
  ld stepVA = 0.045108165;
  printf("-----------------------> y");
  for (int i = 0; i < 42; ++i, x += step) {
    ld a = VA(x);
    // printf("%.7Lf | ", x);
    printf(". ");
    printStar(a, stepVA);
    // if (!isnan(a)) {
    //     mn = fmin(mn, a);
    //     mx = fmax(mx, a);
    // }
    // printf("\n");
  }
  printf("\\/x\n");
  // printf("%.7Lf %.7Lf\n", mn, mx);
}

void calc2() {
  ld x = -pi;
  // ld mn = 1000;
  // ld mx = -1000;
  ld stepLb = 0.021171105000000003;
  printf("-----------------------> y");
  for (int i = 0; i < 42; ++i, x += step) {
    ld a = LB(x);
    // printf("%.7Lf | ", x);
    // printVal(a);
    printf(". ");
    printStar(a, stepLb);
    // if (!isnan(a)) {
    //     mn = fmin(mn, a);
    //     mx = fmax(mx, a);
    // }
    // printf("\n");
  }
  printf("\\/x\n");
  // printf("%.7Lf %.7Lf\n", mn, mx);
}

void calc3() {
  ld x = -pi;
  // ld mn = 1000;
  // ld mx = -1000;
  ld stepQH = 8.510979424999999;
  printf("-----------------------> y");
  for (int i = 0; i < 42; ++i, x += step) {
    ld a = QH(x);
    // printf("%.7Lf | ", x);
    // printVal(a);
    printf(". ");
    printStar(a, stepQH);
    // if (!isnan(a)) {
    //     mn = fmin(mn, a);
    //     mx = fmax(mx, a);
    // }
    // printf("\n");
  }
  printf("\\/x\n");
  // printf("%.7Lf %.7Lf\n", mn, mx);
}

int main() {
  printf("\n");
  calc1();
  printf("\n");
  printf("\n");
  calc2();
  printf("\n");
  printf("\n");
  calc3();
  printf("\n");

  return 0;
}
