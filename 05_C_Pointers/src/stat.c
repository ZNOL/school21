#include <stdio.h>
#define NMAX 10

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int *n) {
    if (getInt(n) || *n > 10 || *n < 0) {
        return 1;
    }

    for (int i = 0; i < *n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int max(int *a, int n) {
    int result = a[0];

    for (int i = 1; i < n; ++i) {
        if (result < a[i]) {
            result = a[i];
        }
    }

    return result;
}

int min(int *a, int n) {
    int result = a[0];

    for (int i = 1; i < n; ++i) {
        if (result > a[i]) {
            result = a[i];
        }
    }

    return result;
}

double mean(int *a, int n) {
    double ans = 0;
    for (int i = 0; i < n; ++i) {
        ans += a[i];
    }

    return ans / n;
}

double variance(int *a, int n) {
    double ans = 0;
    double m = mean(a, n);

    for (int i = 0; i < n; ++i) {
        ans += (a[i] - m) * (a[i] - m);
    }

    return ans / n;
}

void output_result(int max_v, int min_v, double mean_v, double variance_v) {
    printf("%d %d %.6lf %.6lf", max_v, min_v, mean_v, variance_v);
}

int main() {
    int n = -1, data[NMAX];
    if (input(data, &n)) {
        printf("n/a");
        return 0;
    }

    output(data, n);
    output_result(max(data, n), min(data, n), mean(data, n), variance(data, n));

    return 0;
}
