/*
    Search module for the desired value from data array.

    Returned value must be:
        - "even"
        - ">= mean"
        - "<= mean + 3 * sqrt(variance)"
        mean <= x <= mean + 3 * sqrt(variance)
        - "!= 0"
        OR
        0
*/
#include <math.h>
#include <stdio.h>
#define NMAX 30

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int *n) {
    if (getInt(n) || *n > 30 || *n < 0) {
        return 1;
    }

    for (int i = 0; i < *n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

double mean(int *a, int n) {
    double ans = 0;
    for (int i = 0; i < n; ++i) {
        ans += a[i];
    }

    return ans / n;
}

double variance(int *a, int n) {
    double ans = 0;
    double m = mean(a, n);

    for (int i = 0; i < n; ++i) {
        ans += (a[i] - m) * (a[i] - m);
    }

    return ans / n;
}

int search(int *a, int n) {
    double m = mean(a, n);
    double v = variance(a, n);

    for (int i = 0; i < n; ++i) {
        int x = a[i];
        if (x != 0 && x % 2 == 0 && m <= x && x <= m + 3 * sqrt(v)) {
            return x;
        }
    }

    return 0;
}

int main() {
    int n = -1, data[NMAX];
    if (input(data, &n)) {
        printf("n/a");
        return 0;
    }

    printf("%d", search(data, n));

    return 0;
}
