#!/bin/bash

if [ "$#" -ne "1" ]; then
	echo "Wrong amount of armuments"
	exit 1
fi

logFile=$1

count=0
filesNames=""
filesSha=""
if [[ -f $logFile ]]; then
	while IFS= read -r line 
	do
		count=$[$count + 1]		

		countOfWords=$(echo "$line" | wc -w)
		if [ "$countOfWords" -eq "10" ]; then
			fileName=$(echo $line | cut -d " " -f 1)
			sha=$(echo $line | cut -d " " -f 8)
			filesNames=$filesNames" "$fileName
			filesSha=$filesSha" "$sha
		else
			echo "Wrong log file"
			exit 1
		fi 
	done < $logFile
	
	uniqueFiles=$(echo $filesNames | tr " " "\n" | sort -u | wc -l | tr -d " ")
	uniqueShas=$(echo $filesSha | tr " " "\n" | sort -u | wc -l | tr -d " ")
	thirdDig=$[$uniqueShas - $uniqueFiles]

	echo "$count $uniqueFiles $thirdDig"
else 
	echo "Log file $logFile doesn't exist"
	exit 1
fi


