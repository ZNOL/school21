#!/bin/bash

if (( $# < 2 )); then
	echo "No argument supplied"
	exit 1
fi

if [[ -f "$1" ]]; then
	if [[ $1 =~ \.txt$ ]]
	then
		sed -i "" "s/$2/$3/g" $1
	else	
		echo "Wrong file extension"
		exit 1
	fi

	curDateTime=$(date "+%Y-%m-%d %H:%M")

	rawSHA=$(openssl sha256 $1)
	SHA=${rawSHA##*= }

#	fileSize=$(ls -l $1 | cut -d " " -f 8)
	fileSize=$(wc -c $1 | tr $1 " " | tr -d " ")

	logFile=/Users/nanettea/Projects/T02D02-0/src/files.log

	if [[ -f "$logFile" ]]; then
		echo "$1 - $fileSize - $curDateTime - $SHA - sha256" >> $logFile
		echo "Changes have been made"
		exit 0
	else 
		echo "Log file $logFile doesn't exist"
		exit 1
	fi
else

	echo "$1 doesn't exist"
	exit 1
fi

