git add "some file" - добавление файла в git 
git add "deleted file" - фиксируем что файлик удалился/переименовался и только после этого делаем коммит  (нормальные люди могут использовать git rm)
git add . - рекурсивно добавляет все файлы из текущего репозитория

.gitignore - файл с игнорируемыми файлами и дирректориями

git commit - зафиксировать состояние репозитория (-m "Сообщение о коммите")
git commit --amend - исправить последний коммит

git checkout {hash коммита} - изменить текущий HEAD
git checkout master - возвращает в последнее положение

git diff {hash_1} {hash_2} - просмотр изменений между коммитами

git remote add origin(название) git@github.com:{username}/{repository}.git - привязка к удалённому репозиторию

git push -u origin master - запоминает ветку поумолчанию "git push" потом будет пихать менно туда

git clone git@github.com:{username}/{repository}.git - клонирование репозитория с какого-то сервера

git checkout -b sub_branch branch - создает подветку от ветки

git branch -d branch - удаляет ветку

git cherry-pick commitHASH - переносит изменения из любого коммита в текущую ветку
