#include <ctype.h>  // REMOVE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "constants.h"
#include "output.h"
#include "stack.h"

double randfrom(double min, double max) {  // remove later
    double range = (max - min);
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

/* заменяем длинную функцию в букву
1   sin ==  i
2   cos ==  o
3   tan  == t
4   ctg ==  c
    sqrt == q
    ln =    l
5   *
6   +
7   -
8   ^
9   (
10  )
*/

int pr(char c) {
    int res = 0;
    switch (c) {
        case 'e':
            res = -2;
            break;
        case '(':
            res = 1;
            break;
        case '+':
            res = 3;
            break;
        case '-':
            res = 3;
            break;
        case '*':
            res = 4;
            break;
        case '/':
            res = 4;
            break;
        case '^':
            res = 5;
            break;
        default:  // for math funcs
            res = 2;
            break;
    }
    return res;
}

char *infixToPostfix(char *infix) {
    char *postfix = (char *)malloc(MAXLEN * 2 * sizeof(char));
    Stack *stack = initStack();
    int index = 0;
    for (int i = 0; i < strlen(infix); i++) {
        char c = infix[i];
        if (((int)c >= 48) && ((int)c <= 57)) {
            push(stack, c);
        }

        if (c == '(') push(stack, c);
        if (c == ')') {
            index = i;
            while (top(stack) != '(') {
                postfix[index] = top(stack);
                pop(stack);
                index++;
            }
        }

        if ((c == '+') || (c == '-') || (c == '*') || (c == '/')) {
            if ((top(stack) == 'e') || (top(stack) == '(')) push(stack, c);
            if (pr(c) > pr(top(stack))) push(stack, c);
            if (pr(c) <= pr(top(stack))) {
                index = i;
                while ((top(stack) != '(') || (pr(top(stack)) >= pr(c))) {
                    postfix[index] = top(stack);
                    pop(stack);
                    index++;
                }
                push(stack, c);
            }
        }
    }
    while (top(stack) == 'e') {
        postfix[index] = top(stack);
        pop(stack);
        index++;
    }  // добавить cos sin и тд
    return postfix;
}

double getVal(char *postfix, double x) { return randfrom(-1, 1); }

/*
INFIX TO POSFIX
--из обычной записи переводить в постфиксную

GETVAL
--из постфиксной считать значение
*/

double *preCalc(char *postfix) {
    double *y = (double *)malloc(M * sizeof(double));

    int idx = 0;
    for (double x = 0; x <= MY_DOMAIN; x += MY_DOMAIN / M) {
        y[idx++] = getVal(postfix, x);
    }

    return y;
}

void graph() {
    char infix[MAXLEN];
    scanf("%" STR_MAXLEN "s", infix);  // MAXLEN

    char *postfix = infixToPostfix(infix);
    printf("%s\n", postfix);

    double *res = preCalc(infix);

    output(res);  // подавать массив длиной 80 со значениями по иксам

    free(postfix);
    free(res);
}

int main() {
    srand(time(NULL));  // remove later

    graph();

    return 0;
}
