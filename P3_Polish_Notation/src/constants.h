#ifndef SRC_CONSTANTS_H_
#define SRC_CONSTANTS_H_

#include <math.h>

#define N 25
#define M 80

#define STR_MAXLEN "80"
#define MAXLEN 81
#define MY_DOMAIN (double)4 * M_PI  // Левая граница области определения
#define MY_MEANING (double)1        // Область значения по модулю

#endif  // SRC_CONSTANTS_H_
