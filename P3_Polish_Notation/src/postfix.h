#ifndef SRC_POSTFIX_H_
#define SRC_POSTFIX_H_

int my_isdigit(char x);
int ismath(char x);
int isoper(char x);
double getNumber(char *str, int *idx);
int pr(char c);
void operatopPrecessing(const char *infix, char *ch, int *idx);
char *infixToPostfix(char *infix);
double execute(char op, double first, double second);
double getVal(char *postfix, double x);

#endif  // SRC_POSTFIX_H_
