#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "constants.h"
#include "stack.h"

int my_isdigit(char x) { return 48 <= (int)x && (int)x <= 57; }
int ismath(char x) { return strchr("iotcql", x) != NULL; }
int isoper(char x) { return strchr("(+-*/^~", x) != NULL; }

double getNumber(char *str, int *idx) {
    int i = *idx;
    double ans = 0;
    int len = strlen(str);
    while (i < len && my_isdigit(str[i])) {
        ans = 10 * ans + (str[i] - '0');
        if (i + 1 < len && my_isdigit(str[i + 1])) {
            ++i;
        } else {
            *idx = i;
            i = len;
        }
    }
    return ans;
}

int pr(char c) {
    int res = 0;
    switch (c) {
        case 'e':
            res = -2;
            break;
        case '(':
            res = 1;
            break;
        case '+':
            res = 3;
            break;
        case '-':
            res = 3;
            break;
        case '*':
            res = 4;
            break;
        case '/':
            res = 4;
            break;
        case 'i':
            res = 5;
            break;
        case 'o':
            res = 5;
            break;
        case 't':
            res = 5;
            break;
        case 'c':
            res = 5;
            break;
        case 'q':
            res = 5;
            break;
        case 'l':
            res = 5;
            break;
        case '^':
            res = 6;
            break;
        case '~':
            res = 7;
            break;
        default:  // for math funcs
            res = 1;
            break;
    }
    return res;
}

void operatopPrecessing(const char *infix, char *ch, int *idx) {
    char next = infix[*idx];
    if (*ch == '-') {
        if (*idx == 0 || (*idx >= 2 && isoper(infix[*idx - 2]))) {
            *ch = '~';  // унарный минус
        }
    } else if (*ch == 's') {
        if (next == 'i') {
            *ch = 'i';  // sin
            *idx += 3;
        } else if (next == 'q') {
            *ch = 'q';
            *idx += 4;  // sqrt
        }
    } else if (*ch == 'c') {
        if (next == 'o') {
            *ch = 'o';  // cos
        } else if (next == 't') {
            *ch = 'c';  // ctg
        }
        *idx += 3;
    } else if (*ch == 't') {
        if (next == 'a') {
            *ch = 't';  // tan
        }
        *idx += 2;
    } else if (*ch == 'l') {
        if (next == 'n') {
            *ch = 'l';  // ln
            *idx += 2;
        }
    }
}

char *infixToPostfix(char *infix) {
    char *postfix = (char *)calloc(MAXLEN * 2, sizeof(char));
    Stack *stack = initStack();
    int index = 0;
    for (int i = 0; i < strlen(infix); i++) {
        char c, s;
        c = infix[i];
        s = infix[i + 1];
        if ((((int)c >= 48) && ((int)c <= 57)) && !(((int)s >= 48) && ((int)s <= 57))) {
            postfix[index++] = c;
            postfix[index++] = ' ';
        } else if ((((int)c >= 48) && ((int)c <= 57)) && (((int)s >= 48) && ((int)s <= 57))) {
            postfix[index++] = c;
        } else if (c == '(') {
            push(stack, c);
        } else if (c == ')') {
            while (top(stack) != '(') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            pop(stack);
        } else if ((infix[i] == 's') && (infix[i + 1] == 'i') && (infix[i + 2] == 'n')) {
            c = 'i';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);
        } else if ((infix[i] == 'c') && (infix[i + 1] == 'o') && (infix[i + 2] == 's')) {
            c = 'o';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);
        } else if ((infix[i] == 't') && (infix[i + 1] == 'a')) {
            c = 't';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);
        } else if ((infix[i] == 'c') && (infix[i + 1] == 't') && (infix[i + 2] == 'g')) {
            c = 'c';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            postfix[index++] = ' ';
            push(stack, c);
        } else if ((infix[i] == 's') && (infix[i + 1] == 'q') && (infix[i + 2] == 'r') &&
                   (infix[i + 3] == 't')) {
            c = 'q';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);
        } else if ((infix[i] == 'l') && (infix[i + 1] == 'n')) {
            c = 'l';
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);

        } else if ((c == '+') || (c == '-') || (c == '~') || (c == '*') || (c == '/')) {
            while (pr(c) <= pr(top(stack)) && top(stack) != 'e') {
                postfix[index++] = top(stack);
                pop(stack);
            }
            push(stack, c);
        } else if (c == 'x') {
            postfix[index++] = c;
            postfix[index++] = ' ';
        }
    }
    while (top(stack) != 'e') {
        postfix[index++] = top(stack);
        pop(stack);
    }

    postfix[index] = '\0';
    clearStack(stack);
    return postfix;
}

double execute(char op, double first, double second) {
    double res = 0;
    if (op == '~' || op == '-') {
        res = first - second;
    } else if (op == '+') {
        res = first + second;
    } else if (op == '*') {
        res = first * second;
    } else if (op == '/') {
        res = first / second;
    } else if (op == '^') {
        res = pow(first, second);
    } else if (op == 'i') {
        res = sin(second);
    } else if (op == 'o') {
        res = cos(second);
    } else if (op == 't') {
        res = tan(second);
    } else if (op == 'c') {
        res = 1.0 / tan(second);
    } else if (op == 'q') {
        res = sqrt(second);
    } else if (op == 'l') {
        res = log10(second);
    }
    return res;
}

double getVal(char *postfix, double x) {
    Stack *st = initStack();
   3 2+3*
   15
    // printf("x = %lf\n\n", x);
    int idx = 0;
    int postfix_len = strlen(postfix);
    while (idx < postfix_len) {
        char c = postfix[idx];
        double first = 0, second = 0;
        if (my_isdigit(c) || c == 'x') {
            if (c == 'x') {
                push(st, x);
            } else {
                push(st, getNumber(postfix, &idx));
            }
            // printf("!! push %lf \n", top(st));
        } else if ((ismath(c) || c == '~') && !empty(st)) {
            second = top(st);
            pop(st);
            push(st, execute(c, first, second));
            // printf("! CMD=%c | push %lf (f = %lf, s = %lf)\n", c, top(st),first, second);
        } else if (isoper(c)) {
            if (!empty(st)) {
                second = top(st);
                pop(st);
            }
            if (!empty(st)) {
                first = top(st);
                pop(st);
            }
            push(st, execute(c, first, second));
            // printf("! CMD=%c | push %lf (f = %lf, s = %lf)\n", c, top(st),first, second);
        }
        ++idx;
    }

    double res = 2;
    if (!empty(st)) {
        res = top(st);
    }
    // printf("result = %lf\n\n", res);

    clearStack(st);
    return res;
}
