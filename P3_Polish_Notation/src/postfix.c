#include <math.h>
// #include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "constants.h"
#include "stack.h"

int my_isdigit(char x) { return 48 <= (int)x && (int)x <= 57; }

int ismath(char x) { return strchr("iotcql", x) != NULL; }

int isoper(char x) { return strchr("(+-*/^~", x) != NULL; }

double getNumber(char *str, int *idx) {
    int i = *idx;
    double ans = 0;
    int len = strlen(str);
    while (i < len && my_isdigit(str[i])) {
        ans = 10 * ans + (str[i] - '0');
        if (i + 1 < len && my_isdigit(str[i + 1])) {
            ++i;
        } else {
            *idx = i;
            i = len;
        }
    }
    return ans;
}

int pr(char c) {
    int res = 0;
    switch (c) {
        case 'e':
            res = -2;
            break;
        case '(':
            res = 1;
            break;
        case '+':
            res = 3;
            break;
        case '-':
            res = 3;
            break;
        case '*':
            res = 4;
            break;
        case '/':
            res = 4;
            break;
        case '^':
            res = 5;
            break;
        case '~':
            res = 6;
            break;
        default:  // for math funcs
            res = 1;
            break;
    }
    return res;
}

void operatopPrecessing(const char *infix, char *ch, int *idx) {
    char next = infix[*idx];
    if (*ch == '-') {
        if (*idx == 0 || (*idx >= 2 && isoper(infix[*idx - 2]))) {
            *ch = '~';  // унарный минус
        }
    } else if (*ch == 's') {
        if (next == 'i') {
            *ch = 'i';  // sin
            *idx += 3;
        } else if (next == 'q') {
            *ch = 'q';
            *idx += 4;  // sqrt
        }
    } else if (*ch == 'c') {
        if (next == 'o') {
            *ch = 'o';  // cos
        } else if (next == 't') {
            *ch = 'c';  // ctg
        }
        *idx += 3;
    } else if (*ch == 't') {
        if (next == 'a') {
            *ch = 't';  // tan
        }
        *idx += 2;
    } else if (*ch == 'l') {
        if (next == 'n') {
            *ch = 'l';  // ln
            *idx += 2;
        }
    }
}

char *infixToPostfix(char *infix) {
    char *postfix = (char *)calloc(MAXLEN * 3, sizeof(char));
    int postIdx = 0;

    int idx = 0;
    Stack *st = initStack();

    int need_space = 0;
    int infix_len = strlen(infix);
    while (idx < infix_len) {
        char ch = infix[idx++];
        operatopPrecessing(infix, &ch, &idx);

        if (ch == '(' || ismath(ch)) {
            push(st, ch);
        } else if (my_isdigit(ch) || ch == 'x') {
            postfix[postIdx++] = ch;
            need_space = 1;
        } else if (ch == ')') {
            if (need_space) {
                postfix[postIdx++] = ' ';
                need_space = 0;
            }
            while (!empty(st) && top(st) != '(' && !ismath(top(st))) {
                postfix[postIdx++] = top(st);
                pop(st);
            }
            if (ismath(top(st))) {
                postfix[postIdx++] = top(st);
            }
            pop(st);
        } else if (isoper(ch)) {
            if (need_space) {
                postfix[postIdx++] = ' ';
                need_space = 0;
            }
            while (!empty(st) && pr(top(st)) >= pr(ch)) {
                postfix[postIdx++] = top(st);
                pop(st);
            }
            push(st, ch);
        }
    }

    while (top(st) != 'e') {
        postfix[postIdx++] = top(st);
        pop(st);
    }

    clearStack(st);
    return postfix;
}

double execute(char op, double first, double second) {
    double res = 0;
    if (op == '~' || op == '-') {
        res = first - second;
    } else if (op == '+') {
        res = first + second;
    } else if (op == '*') {
        res = first * second;
    } else if (op == '/') {
        res = first / second;
    } else if (op == '^') {
        res = pow(first, second);
    } else if (op == 'i') {
        res = sin(second);
    } else if (op == 'o') {
        res = cos(second);
    } else if (op == 't') {
        res = tan(second);
    } else if (op == 'c') {
        res = 1.0 / tan(second);
    } else if (op == 'q') {
        res = sqrt(second);
    } else if (op == 'l') {
        res = log10(second);
    }
    return res;
}

double getVal(char *postfix, double x) {
    Stack *st = initStack();

    // printf("x = %lf\n\n", x);
    int idx = 0;
    int postfix_len = strlen(postfix);
    while (idx < postfix_len) {
        char c = postfix[idx];
        double first = 0, second = 0;
        if (my_isdigit(c) || c == 'x') {
            if (c == 'x') {
                push(st, x);
            } else {
                push(st, getNumber(postfix, &idx));
            }
            // printf("!! push %lf \n", top(st));
        } else if ((ismath(c) || c == '~') && !empty(st)) {
            second = top(st);
            pop(st);
            push(st, execute(c, first, second));
            // printf("! CMD=%c | push %lf (f = %lf, s = %lf)\n", c, top(st),first, second);
        } else if (isoper(c)) {
            if (!empty(st)) {
                second = top(st);
                pop(st);
            }
            if (!empty(st)) {
                first = top(st);
                pop(st);
            }
            push(st, execute(c, first, second));
            // printf("! CMD=%c | push %lf (f = %lf, s = %lf)\n", c, top(st),first, second);
        }
        ++idx;
    }

    double res = 2;
    if (!empty(st)) {
        res = top(st);
    }
    // printf("result = %lf\n\n", res);

    clearStack(st);
    return res;
}
