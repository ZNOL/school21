#ifndef SRC_STACK_H_
#define SRC_STACK_H_

typedef struct Node {
    double value;
    struct Node *next;
} Node;

typedef struct Stack {
    Node *top;
} Stack;

Stack *initStack();
Node *initNode(double item);
void clearNode(Node *ptr);
void clearStack(Stack *stack);
double top(Stack *stack);
void push(Stack *stack, double x);
void pop(Stack *stack);
int empty(Stack *stack);

#endif  // SRC_STACK_H_
