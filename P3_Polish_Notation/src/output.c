#include <stdio.h>

#include "constants.h"

int inInterval(double my_y, double y) {
    int res = 0;
    double dy = 2 * MY_MEANING / (N - 0.9999) / 2;
    if (y - dy <= my_y && my_y <= y + dy) {
        res = 1;
    }
    return res;
}

void output(double *results) {
    // for (double y = MY_MEANING; y >= -MY_MEANING; y -= 2 * MY_MEANING / (N - 0.9999)) {
    for (double y = -MY_MEANING; y <= MY_MEANING; y += 2 * MY_MEANING / (N - 0.9999)) {
        int idx = 0;
        for (double x = 0; x <= MY_DOMAIN; x += MY_DOMAIN / M) {
            if (inInterval(results[idx++], y)) {
                printf("*");
            } else {
                printf(".");
            }
        }
        printf("\n");
    }
}
