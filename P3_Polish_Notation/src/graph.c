#include <stdio.h>
#include <stdlib.h>

#include "constants.h"
#include "output.h"
#include "postfix.h"
#include "stack.h"

double *preCalc(char *postfix) {
    double *y = (double *)malloc(M * sizeof(double));

    int idx = 0;
    for (double x = 0; x <= MY_DOMAIN; x += MY_DOMAIN / M) {
        y[idx++] = getVal(postfix, x);
    }

    return y;
}

void graph() {
    char infix[MAXLEN] = {0};
    scanf("%" STR_MAXLEN "s", infix);  // MAXLEN

    // printf("%s\n", infix);
    char *postfix = infixToPostfix(infix);
    // printf("%s\n", postfix);

    double *res = preCalc(postfix);

    output(res);  // подавать массив длиной 80 со значениями по иксам

    free(postfix);
    free(res);
}

int main() {
    graph();

    return 0;
}
