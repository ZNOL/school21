#include "stack.h"

#include <stdlib.h>
#include <string.h>

Stack *initStack() { return (Stack *)calloc(1, sizeof(Stack)); }

Node *initNode(double item) {
    Node *res = (Node *)calloc(1, sizeof(Node));
    res->value = item;
    return res;
}

void clearNode(Node *ptr) { free(ptr); }

void clearStack(Stack *stack) {
    Node *ptr = stack->top;
    while (ptr) {
        Node *tmp = ptr->next;
        clearNode(ptr);
        ptr = tmp;
    }
    free(stack);
}

double top(Stack *stack) {
    double res = 'e';
    if (stack->top) {
        res = stack->top->value;
    }
    return res;
}

void push(Stack *stack, double x) {
    Node *new_node = initNode(x);
    new_node->next = stack->top;
    stack->top = new_node;
}

void pop(Stack *stack) {
    Node *ptr = stack->top;
    if (ptr != NULL) {
        stack->top = ptr->next;
        clearNode(ptr);
    }
}

int empty(Stack *stack) { return stack->top == NULL; }
