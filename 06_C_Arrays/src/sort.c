#include <stdio.h>
#define SIZE 10

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void sort(int *a, int n) {
    for (int i = 0; i < n - 1; ++i) {
        int flag = 1;
        for (int j = 1; j < n - i; ++j) {
            if (a[j - 1] > a[j]) {
                swap(&a[j - 1], &a[j]);
                flag = 0;
            }
        }
        if (flag) {
            return;
        }
    }
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main() {
    int a[SIZE];

    if (input(a, SIZE)) {
        printf("n/a");
        return 0;
    }

    sort(a, SIZE);

    output(a, SIZE);

    return 0;
}
