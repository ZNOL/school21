#include <stdio.h>
#define SIZE 10

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void bubbleSort(int *a, int n) {
    for (int i = 0; i < n - 1; ++i) {
        int flag = 1;
        for (int j = 1; j < n - i; ++j) {
            if (a[j - 1] > a[j]) {
                swap(&a[j - 1], &a[j]);
                flag = 0;
            }
        }
        if (flag) {
            return;
        }
    }
}

int partition(int *a, int left, int right) {
    int pivot = a[right];
    int i = left - 1;

    for (int j = left; j < right; ++j) {
        if (a[j] <= pivot) {
            ++i;
            swap(&a[i], &a[j]);
        }
    }

    swap(&a[i + 1], &a[right]);

    return i + 1;
}

void quickSort(int *a, int left, int right) {
    if (left < right) {
        int pi = partition(a, left, right);

        quickSort(a, left, pi - 1);
        quickSort(a, pi + 1, right);
    }
}

void sift_down(int *arr, int n, int i) {
    int parent = i;
    int leftChild = 2 * parent + 1;
    int rightChild = 2 * parent + 2;

    if (leftChild < n && arr[parent] < arr[leftChild]) {
        parent = leftChild;
    }

    if (rightChild < n && arr[parent] < arr[rightChild]) {
        parent = rightChild;
    }

    if (parent != i) {
        swap(&arr[i], &arr[parent]);
        sift_down(arr, n, parent);
    }
}

void heapSort(int *arr, int n) {
    for (int i = n / 2 - 1; i >= 0; --i) {
        sift_down(arr, n, i);
    }

    for (int i = n - 1; i >= 0; --i) {
        swap(&arr[0], &arr[i]);
        sift_down(arr, i, 0);
    }
}

void copyArray(int *source, int *dest, int n) {
    for (int i = 0; i < n; ++i) {
        dest[i] = source[i];
    }
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main() {
    int a[SIZE], b[SIZE];

    if (input(a, SIZE)) {
        printf("n/a");
        return 0;
    }

    copyArray(a, b, SIZE);

    quickSort(a, 0, SIZE - 1);
    output(a, SIZE);

    heapSort(b, SIZE);
    output(b, SIZE);

    return 0;
}
