#include <stdio.h>
#define SIZE 10

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int *n) {
    if (getInt(n) || *n < 0 || *n > SIZE) {
        return 1;
    }

    for (int i = 0; i < *n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int sum_numbers(int *a, int length) {
    int sum = 0;
    for (int i = 0; i < length; ++i) {
        if (a[i] % 2 == 0) {
            sum += a[i];
        }
    }

    return sum;
}

int find_numbers(int *arr, int len, int num, int *ans_len, int *ans) {
    *ans_len = 0;
    for (int i = 0; i < len; ++i) {
        if (arr[i] != 0 && num % arr[i] == 0) {
            ans[(*ans_len)++] = arr[i];
        }
    }
    return 0;
}

/*------------------------------------
        Функция получает массив данных
        через stdin.
        Выдает в stdout особую сумму
        и сформированный массив
        специальных элементов
        (выбранных с помощью найденной суммы):
        это и будет частью ключа
-------------------------------------*/
int main() {
    int n, buff[SIZE];
    if (input(buff, &n)) {
        printf("n/a");
        return 0;
    }

    int tmp = sum_numbers(buff, n);
    if (tmp == 0) {
        printf("n/a");
        return 0;
    }

    int ans_len = 0, ans[SIZE];
    find_numbers(buff, n, tmp, &ans_len, ans);
    if (ans_len == 0) {
        printf("n/a");
        return 0;
    } else {
        printf("%d\n", tmp);
        output(ans, ans_len);
    }

    return 0;
}
