#include <stdio.h>
#define SIZE 10

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *a, int *n) {
    if (getInt(n) || *n < 0 || *n > SIZE) {
        return 1;
    }

    for (int i = 0; i < *n; ++i) {
        if (getInt(a + i)) {
            return 1;
        }
    }
    return 0;
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int getMod(int x, int MOD) { return ((x % MOD) + MOD) % MOD; }

void calc(int *source, int n, int *dest, int diff) {
    for (int i = 0; i < n; ++i) {
        dest[i] = source[getMod(i + diff, n)];
    }
}

int main() {
    int n, a[SIZE], diff = 0;
    if (input(a, &n) || getInt(&diff)) {
        printf("n/a");
        return 0;
    }

    int res[SIZE];
    calc(a, n, res, diff);
    output(res, n);

    return 0;
}
