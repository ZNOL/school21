#include <stdio.h>
#define LEN 100

/*
    Беззнаковая целочисленная длинная арифметика
    с использованием массивов.
    Ввод:
     * 2 длинных числа в виде массивов до 100 элементов
     * В один элемент массива нельзя вводить число > 9
    Вывод:
     * Результат сложения и разности чисел-массивов
    Пример:
     * 1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 6 1
       2 9

       1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 9 0
       1 9 4 4 6 7 4 4 0 7 3 7 0 9 5 5 1 3 2
*/

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input(int *res, int *n) {
    *n = 0;
    int tmp = -1;
    char delim = '-';
    while ((scanf("%d", &tmp)) == 1) {
        if (0 <= tmp && tmp <= 9 && *n < LEN) {
            res[(*n)++] = tmp;
            int flag = scanf("%c", &delim);
            if (delim == '\n' || flag == -1) {
                return 0;
            } else if (delim != ' ') {
                return 1;
            }
        } else {
            return 1;
        }
    }
    return 0;
}

void assign(int *r, int n, int x) {
    for (int i = 0; i < n; ++i) {
        r[i] = x;
    }
}

int sum(int *buff1, int len1, int *buff2, int len2, int *ans, int *len) {
    *len = 0;
    int idx = LEN - 1;
    int i1 = len1 - 1;
    int i2 = len2 - 1;

    while (i1 >= 0 || i2 >= 0) {
        ans[idx] = 0;
        if (i1 >= 0) {
            ans[idx] += buff1[i1--];
        }
        if (i2 >= 0) {
            ans[idx] += buff2[i2--];
        }
        --idx;
        (*len)++;
    }

    for (int i = LEN - 1; i >= 0 && ans[i] != -1; --i) {
        if (ans[i] >= 10) {
            if (i == 0) {
                return 1;
            }

            if (ans[i - 1] == -1) {
                ans[i - 1] = 0;
                (*len)++;
            }

            ans[i] -= 10;
            ans[i - 1] += 1;
        }
    }

    if (*len != 0) {
        return 0;
    } else {
        return 1;
    }
}

int sub(int *first, int len1, int *second, int len2, int *ans, int *len) {
    int idx = LEN - 1;
    int i1 = len1 - 1;
    int i2 = len2 - 1;
    *len = 0;

    while (i1 >= 0 && i2 >= 0) {
        if (first[i1] < second[i2]) {
            int find_idx = i1 - 1;
            while (find_idx >= 0) {
                if (first[find_idx] > 0) {
                    break;
                }
                find_idx--;
            }

            if (find_idx < 0) {
                return 1;
            }

            first[find_idx]--;
            for (int _ = find_idx + 1; _ < i1; ++_) {
                first[_] += 9;
            }
            first[i1] += 10;
        }
        ans[idx--] = first[i1] - second[i2];
        --i1;
        --i2;
    }

    if (i1 < 0 && i2 >= 0) {
        return 1;
    }

    if (i2 < 0 && i1 >= 0) {
        while (i1 >= 0) {
            if (idx < 0) {
                return 1;
            }
            ans[idx--] = first[i1--];
        }
    }

    return 0;
}

void output(int *inp) {
    int flag = 0;
    for (int i = 0; i < LEN; ++i) {
        if (inp[i] != -1) {
            if (inp[i] != 0) {
                flag = 1;
            }

            if (flag) {
                printf("%d ", inp[i]);
            }
        }
    }
    if (!flag) {
        printf("0");
    }
    printf("\n");
}

int main() {
    int len1 = -1, len2 = -1;
    int first[LEN], second[LEN];

    if (input(first, &len1) || input(second, &len2) || len1 == 0 || len2 == 0) {
        printf("n/a");
        return 0;
    }

    int len1_res, res1[LEN];
    int len2_res, res2[LEN];
    assign(res1, LEN, -1);
    assign(res2, LEN, -1);

    if (sum(first, len1, second, len2, res1, &len1_res) || sub(first, len1, second, len2, res2, &len2_res)) {
        printf("n/a");
        return 0;
    }

    output(res1);
    output(res2);

    return 0;
}
