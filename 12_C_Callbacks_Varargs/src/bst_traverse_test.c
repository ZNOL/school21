#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

void addTree_test() {
    int keys[] = {6, 2, 7, 2, 3, 9};
    char *values[] = {"a", "b", "c", "d", "e", "f"};
    const int N = 6;

    Node *root = NULL;
    for (int i = 0; i < N; ++i) {
        addTree(&root, keys[i], values[i]);
    }

    bstree_apply_infix(root, applyf);
    printf("\n");
    bstree_apply_prefix(root, applyf);
    printf("\n");
    bstree_apply_postfix(root, applyf);

    clearTree(&root);
}

int main() {
    addTree_test();

    return 0;
}
