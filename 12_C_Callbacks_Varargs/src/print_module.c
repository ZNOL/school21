#include "print_module.h"

#include <stdio.h>
#include <string.h>
#include <time.h>

char print_char(char ch) { return putchar(ch); }

void print_log(char (*print)(char), char *message) {
    for (int i = 0; i < (int)sizeof(Log_prefix); ++i) {
        print(Log_prefix[i]);
    }
    print(' ');

    time_t timer = time(NULL);
    struct tm *tm_info = localtime(&timer);

    char buffer[26];
    strftime(buffer, 8, "%H:%M:%S", tm_info);
    for (int i = 0; i < 8; ++i) {
        print(buffer[i]);
    }
    print(' ');

    for (int i = 0; i < (int)strlen(message); ++i) {
        print(message[i]);
    }
}
