#include "documentation_module.h"

#include <stdarg.h>
#include <stdlib.h>

int validate(char *data) {
    int validation_result = !strcmp(data, Available_document);
    return validation_result;
}

int *check_available_documentation_module(int (*validate)(char *), int document_count, ...) {
    va_list ap;

    int *res = (int *)malloc(document_count * sizeof(int));
    va_start(ap, document_count);
    for (int i = 0; i < document_count; ++i) {
        char *str = va_arg(ap, char *);
        res[i] = validate(str);
    }
    va_end(ap);

    return res;
}

void output(int *data, int count, ...) {
    va_list ap;

    va_start(ap, count);
    for (int i = 0; i < count; ++i) {
        char *str = va_arg(ap, char *);
        printf("%s %savailable", str, (data[i] == 1) ? "" : "un");
        if (i != count - 1) {
            printf("\n");
        }
    }
    va_end(ap);
}
