#include <stdio.h>
#define N 15
#define M 13

void reset_picture(int picture[N][M], int n, int m) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            picture[i][j] = 0;
        }
    }
}

void paintRow(int picture[N][M], int *arr, int n, int i, int j, int di, int dj) {
    for (int idx = 0; idx < n; ++idx) {
        picture[i][j] = arr[idx];

        i += di;
        j += dj;
    }
}

void make_picture(int picture[N][M], int n, int m) {
    int frame_w[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int frame_h[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int tree_trunk[] = {7, 7, 7, 7};
    int tree_foliage[] = {3, 3, 3, 3};
    int sun_data[6][5] = {{0, 6, 6, 6, 6}, {0, 0, 6, 6, 6}, {0, 0, 6, 6, 6},
                          {0, 6, 0, 0, 6}, {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0}};

    int len_frame_w = sizeof(frame_w) / sizeof(frame_w[0]);
    int len_frame_h = sizeof(frame_h) / sizeof(frame_h[0]);
    int len_tree_t = sizeof(tree_trunk) / sizeof(tree_trunk[0]);
    int len_tree_f = sizeof(tree_foliage) / sizeof(tree_foliage[0]);

    reset_picture(picture, n, m);

    /* Sun */
    for (int i = 1; i <= 5; ++i) {
        paintRow(picture, sun_data[i - 1], 5, i, 7, 0, 1);
    }

    /* Tree foliage */
    paintRow(picture, tree_foliage, len_tree_f, 3, 2, 0, 1);
    paintRow(picture, tree_foliage, len_tree_f, 4, 2, 0, 1);
    paintRow(picture, tree_foliage, len_tree_f, 2, 3, 1, 0);
    paintRow(picture, tree_foliage, len_tree_f, 2, 4, 1, 0);

    /* Tree trunk  */
    paintRow(picture, tree_trunk, len_tree_t, 6, 3, 1, 0);
    paintRow(picture, tree_trunk, len_tree_t, 6, 4, 1, 0);
    paintRow(picture, tree_trunk, len_tree_t, 7, 3, 1, 0);
    paintRow(picture, tree_trunk, len_tree_t, 7, 4, 1, 0);
    paintRow(picture, tree_trunk, len_tree_t, 10, 2, 0, 1);

    /* Frames  */
    paintRow(picture, frame_w, len_frame_w, 0, 0, 0, 1);
    paintRow(picture, frame_w, len_frame_w, 7, 0, 0, 1);
    paintRow(picture, frame_w, len_frame_w, n - 1, 0, 0, 1);

    paintRow(picture, frame_h, len_frame_h, 0, 0, 1, 0);
    paintRow(picture, frame_h, len_frame_h, 0, 6, 1, 0);
    paintRow(picture, frame_h, len_frame_h, 0, m - 1, 1, 0);
}

void print_picture(int picture[N][M], int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", picture[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int main() {
    int picture[N][M];

    make_picture(picture, N, M);
    print_picture(picture, N, M);

    return 0;
}
