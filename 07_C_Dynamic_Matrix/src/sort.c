#include <stdio.h>
#include <stdlib.h>

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

void clearArr(int *arr) { free(arr); }

int *input(int *n) {
    if (getInt(n)) {
        return NULL;
    }

    int *result = (int *)malloc((*n) * sizeof(int));
    if (!result) {
        return NULL;
    }

    for (int i = 0; i < *n; ++i) {
        if (getInt(result + i)) {
            clearArr(result);
            return NULL;
        }
    }
    return result;
}

void swap(int *a, int *b) {
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void bubbleSort(int *a, int n) {
    for (int i = 0; i < n - 1; ++i) {
        int flag = 1;
        for (int j = 1; j < n - i; ++j) {
            if (a[j - 1] > a[j]) {
                swap(&a[j - 1], &a[j]);
                flag = 0;
            }
        }
        if (flag) {
            return;
        }
    }
}

int partition(int *a, int left, int right) {
    int pivot = a[right];
    int i = left - 1;

    for (int j = left; j < right; ++j) {
        if (a[j] <= pivot) {
            ++i;
            swap(&a[i], &a[j]);
        }
    }

    swap(&a[i + 1], &a[right]);

    return i + 1;
}

void quickSort(int *a, int left, int right) {
    if (left < right) {
        int pi = partition(a, left, right);

        quickSort(a, left, pi - 1);
        quickSort(a, pi + 1, right);
    }
}

void sift_down(int *arr, int n, int i) {
    int parent = i;
    int leftChild = 2 * parent + 1;
    int rightChild = 2 * parent + 2;

    if (leftChild < n && arr[parent] < arr[leftChild]) {
        parent = leftChild;
    }

    if (rightChild < n && arr[parent] < arr[rightChild]) {
        parent = rightChild;
    }

    if (parent != i) {
        swap(&arr[i], &arr[parent]);
        sift_down(arr, n, parent);
    }
}

void heapSort(int *arr, int n) {
    for (int i = n / 2 - 1; i >= 0; --i) {
        sift_down(arr, n, i);
    }

    for (int i = n - 1; i >= 0; --i) {
        swap(&arr[0], &arr[i]);
        sift_down(arr, i, 0);
    }
}

void copyArray(int *source, int *dest, int n) {
    for (int i = 0; i < n; ++i) {
        dest[i] = source[i];
    }
}

void output(int *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d", a[i]);
        if (i != n - 1) {
            printf(" ");
        }
    }
}

int main() {
    int *arr = NULL, n = 0;

    arr = input(&n);
    if (arr == NULL) {
        printf("n/a");
        return 0;
    }

    quickSort(arr, 0, n - 1);
    output(arr, n);

    clearArr(arr);

    return 0;
}
