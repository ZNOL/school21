#include <stdio.h>
#include <stdlib.h>

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int **initMatrix(int n, int m) {
    int **matrix = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; ++i) {
        matrix[i] = (int *)malloc(m * sizeof(int));
    }
    return matrix;
}

void clearMatrix(int **matrix, int n) {
    for (int i = 0; i < n; ++i) {
        free(matrix[i]);
    }
    free(matrix);
}

int **input(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    int **matrix = initMatrix(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&matrix[i][j])) {
                clearMatrix(matrix, *n);
                return NULL;
            }
        }
    }

    return matrix;
}

void output(int **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int **sum(int **matrix1, int **matrix2, int n, int m) {
    int **matrix = initMatrix(n, m);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            matrix[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    return matrix;
}

int **prod(int **a, int **b, int l, int m, int n) {
    int **matrix = initMatrix(l, n);

    for (int i = 0; i < l; ++i) {
        for (int j = 0; j < n; ++j) {
            matrix[i][j] = 0;
            for (int r = 0; r < m; ++r) {
                matrix[i][j] += a[i][r] * b[r][j];
            }
        }
    }

    return matrix;
}

int **transp(int **matrix1, int n, int m) {
    int **matrix = initMatrix(m, n);

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            matrix[j][i] = matrix1[i][j];
        }
    }

    return matrix;
}

int main() {
    int mode = -1;
    if (getInt(&mode)) {
        printf("n/a");
        return 0;
    }

    int n1 = -1, m1 = -1;
    int n2 = -1, m2 = -1;
    int **arr1 = NULL, **arr2 = NULL, **arr3 = NULL;
    switch (mode) {
        case 1:
            arr1 = input(&n1, &m1);
            arr2 = input(&n2, &m2);
            if (arr1 == NULL || arr2 == NULL || !(n1 == n2 && m1 == m2)) {
                printf("n/a");
                break;
            }

            arr3 = sum(arr1, arr2, n1, m1);

            output(arr3, n1, m1);

            clearMatrix(arr1, n1);
            clearMatrix(arr2, n1);
            clearMatrix(arr3, n1);

            break;
        case 2:
            arr1 = input(&n1, &m1);
            arr2 = input(&n2, &m2);
            if (arr1 == NULL || arr2 == NULL || !(m1 == n2)) {
                printf("n/a");
                break;
            }

            arr3 = prod(arr1, arr2, n1, m1, m2);

            output(arr3, n1, m2);

            clearMatrix(arr1, n1);
            clearMatrix(arr2, n2);
            clearMatrix(arr3, n1);

            break;
        case 3:
            arr1 = input(&n1, &m1);
            if (arr1 == NULL) {
                printf("n/a");
                break;
            }

            arr2 = transp(arr1, n1, m1);

            output(arr2, m1, n1);

            clearMatrix(arr1, n1);
            clearMatrix(arr2, m1);

            break;
    }

    return 0;
}
