#include <stdio.h>
#include <stdlib.h>
#define SIZE 100

typedef struct Matrix3 {
    int **pointer_array;
    int *values_array;
} Matrix3;

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int input1(int matrix[SIZE][SIZE], int *n, int *m) {
    if (getInt(n) || getInt(m) || !(1 <= *n && *n <= SIZE && 1 <= *m && *m <= SIZE)) {
        return 1;
    }

    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&matrix[i][j])) {
                return 1;
            }
        }
    }

    return 0;
}

void output1(int matrix[SIZE][SIZE], int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int **initMatrix2(int n, int m) {
    int **single_array_matrix = (int **)malloc(n * m * sizeof(int) + n * sizeof(int *));
    int *temp_ptr = (int *)(single_array_matrix + n);

    for (int i = 0; i < n; ++i) {
        single_array_matrix[i] = temp_ptr + m * i;
    }

    return single_array_matrix;
}

void clearMatrix2(int **matrix) { free(matrix); }

int **input2(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    int **matrix = initMatrix2(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&matrix[i][j])) {
                clearMatrix2(matrix);
                return NULL;
            }
        }
    }

    return matrix;
}

void output2(int **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

Matrix3 *initMatrix3(int n, int m) {
    Matrix3 *res = (Matrix3 *)malloc(sizeof(Matrix3));
    res->pointer_array = (int **)malloc(n * sizeof(int *));
    res->values_array = (int *)malloc(n * m * sizeof(int));

    for (int i = 0; i < n; ++i) {
        res->pointer_array[i] = res->values_array + m * i;
    }

    return res;
}

void clearMatrix3(Matrix3 *matrix) {
    free(matrix->values_array);
    free(matrix->pointer_array);
    free(matrix);
}

Matrix3 *input3(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    Matrix3 *matrix = initMatrix3(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&(matrix->pointer_array[i][j]))) {
                clearMatrix3(matrix);
                return NULL;
            }
        }
    }
    return matrix;
}

void output3(Matrix3 *matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix->pointer_array[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int **initMatrix(int n, int m) {
    int **matrix = (int **)malloc(n * sizeof(int *));
    for (int i = 0; i < n; ++i) {
        matrix[i] = (int *)malloc(m * sizeof(int));
    }
    return matrix;
}

void clearMatrix(int **matrix, int n) {
    for (int i = 0; i < n; ++i) {
        free(matrix[i]);
    }
    free(matrix);
}

int **input(int *n, int *m) {
    if (getInt(n) || getInt(m) || *n <= 0 || *m <= 0) {
        return NULL;
    }

    int **matrix = initMatrix(*n, *m);
    for (int i = 0; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            if (getInt(&matrix[i][j])) {
                clearMatrix(matrix, *n);
                return NULL;
            }
        }
    }

    return matrix;
}

void output(int **matrix, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            printf("%d", matrix[i][j]);
            if (j != m - 1) {
                printf(" ");
            }
        }
        if (i != n - 1) {
            printf("\n");
        }
    }
}

int main() {
    int mode = -1;
    if (getInt(&mode)) {
        printf("n/a");
        return 0;
    }

    int n = -1, m = -1, **arr = NULL;
    Matrix3 *arr2 = NULL;
    int static_arr[SIZE][SIZE];
    switch (mode) {
        case 1:
            if (input1(static_arr, &n, &m)) {
                printf("n/a");
                break;
            }

            output1(static_arr, n, m);
            break;
        case 2:
            arr = input2(&n, &m);
            if (!arr) {
                printf("n/a");
                break;
            }

            output2(arr, n, m);
            clearMatrix2(arr);
            break;
        case 3:
            arr2 = input3(&n, &m);
            if (!arr2) {
                printf("n/a");
                break;
            }

            output3(arr2, n, m);
            clearMatrix3(arr2);
            break;
        case 4:
            arr = input(&n, &m);
            if (!arr) {
                printf("n/a");
                break;
            }

            output(arr, n, m);
            clearMatrix(arr, n);
            break;
    }

    return 0;
}
