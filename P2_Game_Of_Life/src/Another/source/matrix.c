#include "../headers/matrix.h"

#include <stdio.h>
#include <stdlib.h>

#include "../headers/myinput.h"

Matrix *initMatrix(int n, int m) {
    Matrix *matrix = (Matrix *)malloc(sizeof(Matrix));
    matrix->n = n;
    matrix->m = m;
    matrix->base = (char **)malloc(n * sizeof(char *));
    for (int i = 0; i < n; ++i) {
        matrix->base[i] = (char *)malloc(m * sizeof(char));
    }
    return matrix;
}

void clearMatrix(Matrix *matrix) {
    for (int i = 0; i < matrix->n; ++i) {
        free(matrix->base[i]);
    }
    free(matrix->base);
    free(matrix);
}

void assignMatrix(Matrix *matrix, char x) {
    for (int i = 0; i < matrix->n; ++i) {
        for (int j = 0; j < matrix->m; ++j) {
            matrix->base[i][j] = x;
        }
    }
}

Matrix *input(int n, int m) {
    Matrix *matrix = initMatrix(n, m);
    assignMatrix(matrix, 0);
    int flag = 1;
    for (int i = 0; i < n && flag; ++i) {
        for (int j = 0; j < m && flag; ++j) {
            if (getChar(&matrix->base[i][j])) {
                //                clearMatrix(matrix);
                flag = 0;
            }
        }
    }
    return matrix;
}

void output(Matrix *matrix) {
    printf("\033[0d\033[2J");
    for (int i = -1; i <= matrix->n; ++i) {
        for (int j = -1; j <= matrix->m; ++j) {
            if ((0 <= i && i < matrix->n) && (0 <= j && j < matrix->m)) {
                if (matrix->base[i][j] == 0) {
                    printf("▢");
                } else {
                    printf("▣");
                }
            } else {
                printf("◆");
            }
        }
        printf("\n");
    }
}
