#include "../headers/matrix.h"

int mod(int x, int MOD) { return ((x % MOD) + MOD) % MOD; }

int alive(int i, int j, Matrix *matrix) {
    int count = -matrix->base[i][j];
    for (int l = -1; l <= 1; ++l) {
        for (int k = -1; k <= 1; ++k) {
            count += matrix->base[mod(i + l, matrix->n)][mod(j + k, matrix->m)];
        }
    }

    int flag = 0;
    if (matrix->base[i][j] == 0 && count == 3) {
        flag = 1;
    } else if (matrix->base[i][j] == 1 && (count == 2 || count == 3)) {
        flag = 1;
    }
    return flag;
}

void calc(Matrix *matrix_in, Matrix *matrix_out) {
    assignMatrix(matrix_out, 0);
    for (int i = 0; i < matrix_in->n; ++i) {
        for (int j = 0; j < matrix_in->m; ++j) {
            matrix_out->base[i][j] = alive(i, j, matrix_in);
        }
    }
}
