#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../headers/calc.h"
#include "../headers/matrix.h"
#include "../headers/myconstants.h"
#include "../headers/myinput.h"

int askPlayer(unsigned int *x) {
    int flag = 0;
    if (kbhit()) {
        char move = readch();
        switch (move) {
            case '+':
                if (*x > 1000) {
                    *x -= 1000;
                }
                break;
            case '-':
                if (*x < INT_MAX - 2000) {
                    *x += 1000;
                }
                break;
            case 'q':
                flag = 1;
                break;
        }
    }

    return flag;
}

void game() {
    int n = H, m = W;

    Matrix *field = input(n, m);
    Matrix *temp_field = initMatrix(n, m);
    Matrix *tmp = NULL;
    init_keyboard();
    unsigned int speed = 200000;
    while (1) {
        output(field);

        calc(field, temp_field);

        tmp = field;
        field = temp_field;
        temp_field = tmp;

        usleep(speed);

        if (askPlayer(&speed)) {
            break;
        }
    }

    clearMatrix(field);
    clearMatrix(temp_field);
    close_keyboard();
}

int main() {
    game();

    return 0;
}
