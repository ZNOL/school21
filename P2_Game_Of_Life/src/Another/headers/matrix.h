#pragma once

typedef struct Matrix {
  int n;
  int m;
  char **base;
} Matrix;

Matrix *initMatrix(int n, int m);
void clearMatrix(Matrix *matrix);
Matrix *input(int n, int m);
void output(Matrix *matrix);
void assignMatrix(Matrix *matrix, char x);
