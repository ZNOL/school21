from random import randint


def gen_set(bact):
    s = set()
    for i in range(bact):
        x = randint(0, 24)
        y = randint(0, 79)
        s.add((x, y))
    return s

name = str(randint(0, 999)) + 'map'
for idx in range(1, 5 + 1):
    matrix = [['0' for j in range(80)] for i in range(25)]

    for point in gen_set(randint(1600, 1800)):
        matrix[point[0]][point[1]] = '1'

    f = open(f'{name}{idx}.txt', 'w')
    for row in matrix:
        print(*row, sep='', file=f)
    f.close()

