#include <stddef.h>
#include <stdio.h>

#include "../data_libs/data_io.h"
#include "decision.h"

int main() {
    int n;
    double *data = input(&n);
    if (data == NULL) {
        printf("n/a");
        return 0;
    }

    if (make_decision(data, n)) {
        printf("YES");
    } else {
        printf("NO");
    }
    return 0;
}
