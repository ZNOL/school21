#include <stddef.h>
#include <stdio.h>

#include "../data_libs/data_io.h"
#include "data_process.h"

int main() {
    int n;
    double *data = input(&n);
    if (data == NULL) {
        printf("ERROR");
        return 0;
    }

    if (normalization(data, n)) {
        output(data, n);
    } else {
        printf("ERROR");
    }

    return 0;
}
