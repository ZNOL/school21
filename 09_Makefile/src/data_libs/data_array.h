#ifndef SRC_DATA_LIBS_DATA_ARRAY_H_
#define SRC_DATA_LIBS_DATA_ARRAY_H_

double *initArray(int n);
void clearArray(double *arr);

#endif  // SRC_DATA_LIBS_DATA_ARRAY_H_
