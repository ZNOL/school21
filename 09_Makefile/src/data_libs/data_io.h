#ifndef SRC_DATA_LIBS_DATA_IO_H_
#define SRC_DATA_LIBS_DATA_IO_H_

int getInt(int *res);
int getDouble(double *res);
double *input(int *n);
void output(double *data, int n);

#endif  // SRC_DATA_LIBS_DATA_IO_H_
