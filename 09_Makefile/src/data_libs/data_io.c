#include "data_io.h"

#include <stdio.h>

#include "data_array.h"

int getInt(int *res) {
    long double r;
    if ((scanf("%Lf", &r)) == 1) {
        if ((int)r == r) {
            *res = r;
            return 0;
        } else {
            return 1;
        }
    } else {
        return 1;
    }
}

int getDouble(double *res) {
    if ((scanf("%lf", res)) == 1) {
        return 0;
    } else {
        return 1;
    }
}

double *input(int *n) {
    if (getInt(n) || *n < 0) {
        return NULL;
    }

    double *arr = initArray(*n);
    for (int i = 0; i < *n; ++i) {
        if (getDouble(arr + i)) {
            clearArray(arr);
            return NULL;
        }
    }

    return arr;
}

void output(double *a, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%.2lf", a[i]);
        if (i != n - 1) {
            printf(" ");
        }
    }
}
