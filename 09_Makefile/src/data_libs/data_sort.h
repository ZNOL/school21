#ifndef SRC_DATA_LIBS_DATA_SORT_H_
#define SRC_DATA_LIBS_DATA_SORT_H_

int cmp(const void *a, const void *b);
void sort(double *arr, int n);

#endif  // SRC_DATA_LIBS_DATA_SORT_H_
