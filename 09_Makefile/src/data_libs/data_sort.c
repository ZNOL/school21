#include "data_sort.h"

#include <stdlib.h>

int cmp(const void *a, const void *b) {
    double A = *((double *)a), B = *((double *)b);

    if (A > B) {
        return 1;
    } else if (A < B) {
        return -1;
    } else {
        return 0;
    }
}

void sort(double *arr, int n) { qsort(arr, n, sizeof(double), cmp); }
