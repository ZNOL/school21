#include "data_stat.h"

double max(double *a, int n) {
    double result = a[0];

    for (int i = 1; i < n; ++i) {
        if (result < a[i]) {
            result = a[i];
        }
    }

    return result;
}

double min(double *a, int n) {
    double result = a[0];

    for (int i = 1; i < n; ++i) {
        if (result > a[i]) {
            result = a[i];
        }
    }

    return result;
}

double mean(double *a, int n) {
    double ans = 0;
    for (int i = 0; i < n; ++i) {
        ans += a[i];
    }

    return ans / n;
}

double variance(double *a, int n) {
    double ans = 0;
    double m = mean(a, n);

    for (int i = 0; i < n; ++i) {
        ans += (a[i] - m) * (a[i] - m);
    }

    return ans / n;
}
