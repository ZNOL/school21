#include "data_array.h"

#include <stdlib.h>

double *initArray(int n) { return (double *)malloc(n * sizeof(double)); }

void clearArray(double *arr) {
    if (arr) {
        free(arr);
    }
}
