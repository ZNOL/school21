#include <stddef.h>
#include <stdio.h>

#include "../data_libs/data_io.h"
#include "../data_libs/data_sort.h"
#include "../data_module/data_process.h"
#include "../yet_another_decision_module/decision.h"

int main() {
    int n;
    printf("LOAD DATA...\n");
    double *data = input(&n);
    if (data == NULL) {
        printf("n/a");
        return 0;
    }

    printf("RAW DATA:\n\t");
    output(data, n);

    printf("\nNORMALIZED DATA:\n\t");
    if (!normalization(data, n)) {
        printf("ERROR");
        return 0;
    }
    output(data, n);

    printf("\nSORTED NORMALIZED DATA:\n\t");
    sort(data, n);
    output(data, n);

    printf("\nFINAL DECISION:\n\t");
    if (make_decision(data, n)) {
        printf("YES");
    } else {
        printf("NO");
    }

    return 0;
}
